<?php

namespace Bphtb\Form\Setting;

class FormatPesanFrm extends \Zend\Form\Form {

    public function __construct() {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 's_id_format_pesan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 's_id_format_pesan'
            )
        ));

        $this->add(array(
            'name' => 's_id_jns_pesan',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                // 'empty_option' => 'Silahkan pilih',
                'value_options'=> [
                    "1" => "BERKAS"
                ],
            ),
            'attributes' => array(
                'id' => 's_id_jns_pesan',
                'class' => 'form-control',
                'required' => true,
            )
        ));

        $this->add(array(
            'name' => 's_isian_pesan',
            'type' => 'text',
            'attributes' => array(
                'id' => 's_isian_pesan',
                'class' => 'form-control',
                'rows' => 5,
                'required' => true
            )
        ));

        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
