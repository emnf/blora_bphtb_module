<?php

namespace Bphtb\Helper;

use Zend\Form\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CustomHelper extends AbstractHelper implements ServiceLocatorAwareInterface {

    protected $message = null;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function getMessage() {
        if ($this->message === null) {
            // for example, get the default value from app config
            $sm = $this->getServiceLocator()->getServiceLocator();
            $config = $sm->get('Sessi_Manager')->getStorage();
            $this->setMessage($config);
        }
        return $this->message;
    }

    public function __invoke() {
        $message = $this->getMessage();
        return $message;
    }

}
