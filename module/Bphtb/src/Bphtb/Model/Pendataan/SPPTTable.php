<?php

namespace Bphtb\Model\Pendataan;

use Bphtb\Helper\Spop\ReffSpopHelper;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class SPPTTable extends AbstractTableGateway
{

    protected $table = 'SPPT';


    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SPPTBase());
        $this->initialize();
    }
    // ======================= CETAK SPOP / LSPOP
    public function cek_kd_propinsi()
    {
        $sql = "SELECT * FROM REF_PROPINSI";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();

        return $res;
    }

    public function cek_kd_dati()
    {
        $sql = "SELECT * FROM REF_DATI2";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();

        return $res;
    }

    public function cek_min_max_op($nopcari)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];

        $sql = "SELECT A.KD_PROPINSI, 
                        A.KD_DATI2,
                        A.KD_KECAMATAN,
                        A.KD_KELURAHAN,
                        A.KD_BLOK, 
                        LPAD(MIN(to_number(A.NO_URUT)), 4, '0') AS MIN_NO_URUT, 
                        LPAD(MAX(to_number(A.NO_URUT)), 4, '0') AS MAX_NO_URUT,
                        COUNT(*) as JML_OP,
                        (SELECT COUNT(*) FROM DAT_OP_BANGUNAN B WHERE  
                            B.KD_PROPINSI = A.KD_PROPINSI
                            AND B.KD_DATI2 = A.KD_DATI2
                            AND B.KD_KECAMATAN = A.KD_KECAMATAN
                            AND B.KD_KELURAHAN = A.KD_KELURAHAN
                            AND B.KD_BLOK = A.KD_BLOK ) AS JML_OP_BNG
                FROM DAT_OBJEK_PAJAK A
                    WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            GROUP BY A.KD_PROPINSI, A.KD_DATI2,A.KD_KECAMATAN,A.KD_KELURAHAN,A.KD_BLOK
            ORDER BY A.KD_PROPINSI, A.KD_DATI2,A.KD_KECAMATAN,A.KD_KELURAHAN,A.KD_BLOK ASC
                    
                                
            ";

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();

        return $res;
    }

    public function cetakdataspop_lspop($nopcari, $nourut1, $nourut2)
    {
        //$nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        //$NO_URUT = $nop[5];
        //$KD_JNS_OP = $nop[6];

        $sql = "SELECT 
                    A.NO_FORMULIR_SPOP,
                    A.JNS_TRANSAKSI_OP,
                    A.KD_PROPINSI,A.KD_DATI2, A.KD_KECAMATAN, A.KD_KELURAHAN, A.KD_BLOK, A.NO_URUT, A.KD_JNS_OP,
                    A.JALAN_OP, 
                    A.BLOK_KAV_NO_OP,
                    B.NM_KELURAHAN, B2.NM_KECAMATAN,
                    A.RW_OP, A.RT_OP,
                    A.KD_STATUS_WP,
                    A.SUBJEK_PAJAK_ID, A.NIP_PENDATA, A.NIP_PEMERIKSA_OP,
                    TO_CHAR (A.TGL_PENDATAAN_OP, 'YYYY-MM-DD') as TGL_PENDATAAN_OP,
                    TO_CHAR (A.TGL_PEMERIKSAAN_OP, 'YYYY-MM-DD') as TGL_PEMERIKSAAN_OP,
                    C.STATUS_PEKERJAAN_WP,
                    C.NM_WP,C.NPWP,
                    C.JALAN_WP, C.BLOK_KAV_NO_WP,
                    C.KELURAHAN_WP, C.RW_WP, C.RT_WP,
                    C.KOTA_WP, C.KD_POS_WP,
                    C.SUBJEK_PAJAK_ID AS NOKTP,
                    D.LUAS_BUMI,
                    D.KD_ZNT,
                    D.JNS_BUMI,
                    E.NO_FORMULIR_LSPOP,
                    E.JNS_TRANSAKSI_BNG,
                    E.NO_BNG AS BANGUNAN_KE,
                    E.KD_JPB AS JNS_PENGGUNAAN_BANGUNAN,
                    E.LUAS_BNG AS LUAS_BANGUNAN,
                    E.JML_LANTAI_BNG AS JUMLAH_LANTAI,
                    E.THN_DIBANGUN_BNG AS TAHUN_DIBANGUN,
                    E.THN_RENOVASI_BNG AS TAHUN_DIRENOVASI,
                    E.KONDISI_BNG AS KONDISI_PADA_UMUMNYA,
                    E.JNS_KONSTRUKSI_BNG AS KONSTRUKSI,
                    E.JNS_ATAP_BNG AS ATAP,
                    E.KD_DINDING AS DINDING,
                    E.KD_LANTAI AS LANTAI,
                    E.KD_LANGIT_LANGIT AS LANGIT_LANGIT,
                    E.NILAI_SISTEM_BNG AS NILAI_SISTEM,
                    E.KD_PROPINSI as KD_PROPINSI_BNG,
                    E.KD_DATI2 as KD_DATI2_BNG, 
                    E.KD_KECAMATAN as KD_KECAMATAN_BNG, 
                    E.KD_KELURAHAN as KD_KELURAHAN_BNG, 
                    E.KD_BLOK as KD_BLOK_BNG, 
                    E.NO_URUT as NO_URUT_BNG, 
                    E.KD_JNS_OP as KD_JNS_OP_BNG,
                    TO_CHAR (E.TGL_PENDATAAN_BNG, 'YYYY-MM-DD') as TGL_PENDATAAN_BNG
                    
                    FROM DAT_OBJEK_PAJAK A
                    LEFT JOIN REF_KELURAHAN B ON A.KD_PROPINSI = B.KD_PROPINSI 
                                    AND A.KD_DATI2 = B.KD_DATI2 
                                    AND A.KD_KECAMATAN = B.KD_KECAMATAN 
                                    AND A.KD_KELURAHAN = B.KD_KELURAHAN
                    LEFT JOIN REF_KECAMATAN B2 ON B2.KD_PROPINSI = A.KD_PROPINSI 
                                    AND B2.KD_DATI2 = A.KD_DATI2 
                                    AND B2.KD_KECAMATAN = A.KD_KECAMATAN                
                    LEFT JOIN DAT_SUBJEK_PAJAK C ON A.SUBJEK_PAJAK_ID = C.SUBJEK_PAJAK_ID
                    LEFT JOIN DAT_OP_BUMI D ON A.KD_PROPINSI = D.KD_PROPINSI 
                                    AND A.KD_DATI2 = D.KD_DATI2 
                                    AND A.KD_KECAMATAN = D.KD_KECAMATAN 
                                    AND A.KD_KELURAHAN = D.KD_KELURAHAN 
                                    AND A.KD_BLOK = D.KD_BLOK 
                                    AND A.NO_URUT = D.NO_URUT
                                    AND A.KD_JNS_OP = D.KD_JNS_OP
                    LEFT JOIN DAT_OP_BANGUNAN E ON A.KD_PROPINSI = E.KD_PROPINSI 
                                    AND A.KD_DATI2 = E.KD_DATI2 
                                    AND A.KD_KECAMATAN = E.KD_KECAMATAN 
                                    AND A.KD_KELURAHAN = E.KD_KELURAHAN 
                                    AND A.KD_BLOK = E.KD_BLOK 
                                    AND A.NO_URUT = E.NO_URUT
                                    AND A.KD_JNS_OP = E.KD_JNS_OP
                    
                    WHERE A.KD_PROPINSI = '" . $KD_PROPINSI . "'
                    AND A.KD_DATI2 = '" . $KD_DATI2 . "' 
                    AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "'
                    AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
                    AND A.KD_BLOK = '" . $KD_BLOK . "' 
                    AND A.NO_URUT BETWEEN '" . $nourut1 . "' AND '" . $nourut2 . "' 
                    ORDER BY A.KD_PROPINSI,A.KD_DATI2, A.KD_KECAMATAN, A.KD_KELURAHAN, A.KD_BLOK, A.NO_URUT, A.KD_JNS_OP ASC "; //AND A.THN_PAJAK_SPPT='".$tahuncari."'

        /* LEFT JOIN DAT_FASILITAS_BANGUNAN F ON E.KD_PROPINSI = F.KD_PROPINSI 
                                    AND E.KD_DATI2 = F.KD_DATI2 
                                    AND E.KD_KECAMATAN = F.KD_KECAMATAN 
                                    AND E.KD_KELURAHAN = F.KD_KELURAHAN 
                                    AND E.KD_BLOK = F.KD_BLOK 
                                    AND E.NO_URUT = F.NO_URUT
                                    AND E.KD_JNS_OP = F.KD_JNS_OP
                                    AND E.NO_BNG = F.NO_BNG */

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);

        return $statement->execute();
    }

    public function cetakdataspop_lspop_count($nopcari, $nourut1, $nourut2)
    {
        //$nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        //$NO_URUT = $nop[5];
        //$KD_JNS_OP = $nop[6];

        $sql = "SELECT 
                    count(*) as jml


                    FROM DAT_OBJEK_PAJAK A
                    LEFT JOIN REF_KELURAHAN B ON A.KD_PROPINSI = B.KD_PROPINSI 
                                    AND A.KD_DATI2 = B.KD_DATI2 
                                    AND A.KD_KECAMATAN = B.KD_KECAMATAN 
                                    AND A.KD_KELURAHAN = B.KD_KELURAHAN
                    LEFT JOIN DAT_SUBJEK_PAJAK C ON A.SUBJEK_PAJAK_ID = C.SUBJEK_PAJAK_ID
                    LEFT JOIN DAT_OP_BUMI D ON A.KD_PROPINSI = D.KD_PROPINSI 
                                    AND A.KD_DATI2 = D.KD_DATI2 
                                    AND A.KD_KECAMATAN = D.KD_KECAMATAN 
                                    AND A.KD_KELURAHAN = D.KD_KELURAHAN 
                                    AND A.KD_BLOK = D.KD_BLOK 
                                    AND A.NO_URUT = D.NO_URUT
                                    AND A.KD_JNS_OP = D.KD_JNS_OP
                    LEFT JOIN DAT_OP_BANGUNAN E ON A.KD_PROPINSI = E.KD_PROPINSI 
                                    AND A.KD_DATI2 = E.KD_DATI2 
                                    AND A.KD_KECAMATAN = E.KD_KECAMATAN 
                                    AND A.KD_KELURAHAN = E.KD_KELURAHAN 
                                    AND A.KD_BLOK = E.KD_BLOK 
                                    AND A.NO_URUT = E.NO_URUT
                                    AND A.KD_JNS_OP = E.KD_JNS_OP
                    
                        WHERE A.KD_PROPINSI = '" . $KD_PROPINSI . "'
    AND             A.KD_DATI2 = '" . $KD_DATI2 . "' 
            AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND A.KD_BLOK = '" . $KD_BLOK . "' 
            AND A.NO_URUT BETWEEN '" . $nourut1 . "' AND '" . $nourut2 . "'            
            "; //AND A.THN_PAJAK_SPPT='".$tahuncari."'

        /* LEFT JOIN DAT_FASILITAS_BANGUNAN F ON E.KD_PROPINSI = F.KD_PROPINSI 
                                    AND E.KD_DATI2 = F.KD_DATI2 
                                    AND E.KD_KECAMATAN = F.KD_KECAMATAN 
                                    AND E.KD_KELURAHAN = F.KD_KELURAHAN 
                                    AND E.KD_BLOK = F.KD_BLOK 
                                    AND E.NO_URUT = F.NO_URUT
                                    AND E.KD_JNS_OP = F.KD_JNS_OP
                                    AND E.NO_BNG = F.NO_BNG */

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();



        return $res['JML'];
    }

    public function cekjml_bangunan($nopcari)
    {
        //$nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT 
                    count(*) as jml FROM DAT_OP_BANGUNAN 
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'  
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'       
            ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();



        return $res['JML'];
    }

    public function dat_fasilitas_bangunan($nopcari, $nobangunan, $kdfasilitas)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT 
                    KD_PROPINSI,
                    KD_DATI2,
                    KD_KECAMATAN,
                    KD_KELURAHAN,
                    KD_BLOK,
                    NO_URUT,
                    KD_JNS_OP,
                    NO_BNG,
                    KD_FASILITAS,
                    JML_SATUAN
                    

                    FROM DAT_FASILITAS_BANGUNAN
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'  
                        AND KD_FASILITAS = '" . $kdfasilitas . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function jml_fasilitas_bangunan($nopcari, $nobangunan, $kdfasilitas)
    {
        //$nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT 
                    sum(JML_SATUAN) as jml 
                        FROM DAT_FASILITAS_BANGUNAN
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'  
                        AND KD_FASILITAS IN ('" . $kdfasilitas . "')       
            ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();



        return $res['JML'];
    }

    public function dat_jpb2($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB2
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb3($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB3
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb4($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB4
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb5($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB5
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb6($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB6
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb7($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB7
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb8($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB8
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb9($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB9
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb12($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB12
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb13($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB13
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb14($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB14
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb15($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB15
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function dat_jpb16($nopcari, $nobangunan)
    {

        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "SELECT * FROM DAT_JPB16
                    
                        WHERE KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND KD_DATI2 = '" . $KD_DATI2 . "' 
            AND KD_KECAMATAN = '" . $KD_KECAMATAN . "'
            AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' 
            AND KD_BLOK = '" . $KD_BLOK . "' 
            AND NO_URUT = '" . $NO_URUT . "'    
                        AND KD_JNS_OP = '" . $KD_JNS_OP . "'   
                        AND NO_BNG = '" . $nobangunan . "'    
            ";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    // ======================= END CETAK SPOP / LSPOP

    //Mencari Nilai NOP dari database PBB
    public function temukanData(SPPTBase $spt)
    {

        $nop = explode('.', $spt->t_nopbphtbsppt);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "select A.*, B.NM_KECAMATAN, C.NM_KELURAHAN, D.NILAI_PER_M2_TANAH, E.NILAI_PER_M2_BNG, F.JALAN_OP, F.BLOK_KAV_NO_OP, F.RW_OP, F.RT_OP
                from SPPT A
                LEFT JOIN REF_KECAMATAN B ON B.KD_PROPINSI = A.KD_PROPINSI AND B.KD_DATI2 = A.KD_DATI2 AND B.KD_KECAMATAN = A.KD_KECAMATAN
                LEFT JOIN REF_KELURAHAN C ON C.KD_PROPINSI = A.KD_PROPINSI AND C.KD_DATI2 = A.KD_DATI2 AND C.KD_KECAMATAN = A.KD_KECAMATAN AND C.KD_KELURAHAN = A.KD_KELURAHAN
                LEFT JOIN KELAS_TANAH D ON D.KD_KLS_TANAH = A.KD_KLS_TANAH
                LEFT JOIN KELAS_BANGUNAN E ON E.KD_KLS_BNG = A.KD_KLS_BNG
                LEFT JOIN DAT_OBJEK_PAJAK F ON F.KD_PROPINSI = A.KD_PROPINSI AND F.KD_DATI2 = A.KD_DATI2 AND F.KD_KECAMATAN = A.KD_KECAMATAN AND F.KD_KELURAHAN = A.KD_KELURAHAN AND F.KD_BLOK = A.KD_BLOK AND F.NO_URUT = A.NO_URUT AND F.KD_JNS_OP = A.KD_JNS_OP
                where A.KD_JNS_OP = '" . $KD_JNS_OP . "' AND A.NO_URUT = '" . $NO_URUT . "' AND A.KD_BLOK = '" . $KD_BLOK . "' AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "' AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "' AND A.KD_DATI2 = '" . $KD_DATI2 . "' AND A.KD_PROPINSI ='" . $KD_PROPINSI . "' AND A.THN_PAJAK_SPPT='" . $spt->t_thnsppt . "'";


        /*$sql = "select A.*, B.NM_KECAMATAN, C.NM_KELURAHAN, D.NILAI_PER_M2_TANAH, E.NILAI_PER_M2_BNG, F.JALAN_OP, F.BLOK_KAV_NO_OP, F.RW_OP, F.RT_OP
                from SPPT A
                LEFT JOIN REF_KECAMATAN B ON B.KD_PROPINSI = A.KD_PROPINSI AND B.KD_DATI2 = A.KD_DATI2 AND B.KD_KECAMATAN = A.KD_KECAMATAN
                LEFT JOIN REF_KELURAHAN C ON C.KD_PROPINSI = A.KD_PROPINSI AND C.KD_DATI2 = A.KD_DATI2 AND C.KD_KECAMATAN = A.KD_KECAMATAN AND C.KD_KELURAHAN = A.KD_KELURAHAN
                LEFT JOIN KELAS_TANAH D ON D.KD_KLS_TANAH = A.KD_KLS_TANAH
                LEFT JOIN KELAS_BANGUNAN E ON E.KD_KLS_BNG = A.KD_KLS_BNG
                LEFT JOIN DAT_OBJEK_PAJAK F ON F.KD_PROPINSI = A.KD_PROPINSI AND F.KD_DATI2 = A.KD_DATI2 AND F.KD_KECAMATAN = A.KD_KECAMATAN AND F.KD_KELURAHAN = A.KD_KELURAHAN AND F.KD_BLOK = A.KD_BLOK AND F.NO_URUT = A.NO_URUT AND F.KD_JNS_OP = A.KD_JNS_OP
                where A.KD_PROPINSI||A.KD_DATI2||A.KD_KECAMATAN||A.KD_KELURAHAN||A.KD_BLOK
                ||A.NO_URUT||A.KD_JNS_OP='" . $spt->t_nopbphtbsppt . "' and A.THN_PAJAK_SPPT='" . $spt->t_thnsppt . "'";*/
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataInfoop($nopcari, $tahuncari)
    {
        //$nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $nop = explode('.', $nopcari);

        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];

        $sql = "select A.*,
                    B.NM_KECAMATAN, 
                    C.NM_KELURAHAN, 
                    D.NILAI_PER_M2_TANAH, 
                    E.NILAI_PER_M2_BNG, 
                    F.JALAN_OP, 
                    F.BLOK_KAV_NO_OP, 
                    F.RW_OP, F.RT_OP 
			from SPPT A 
			LEFT JOIN REF_KECAMATAN B ON B.KD_PROPINSI||B.KD_DATI2||B.KD_KECAMATAN = A.KD_PROPINSI||A.KD_DATI2||A.KD_KECAMATAN 
                        LEFT JOIN REF_KELURAHAN C ON C.KD_PROPINSI||C.KD_DATI2||C.KD_KECAMATAN||C.KD_KELURAHAN = A.KD_PROPINSI||A.KD_DATI2||A.KD_KECAMATAN||A.KD_KELURAHAN   
			LEFT JOIN KELAS_TANAH D ON D.KD_KLS_TANAH = A.KD_KLS_TANAH 
			LEFT JOIN KELAS_BANGUNAN E ON E.KD_KLS_BNG = A.KD_KLS_BNG 
			LEFT JOIN DAT_OBJEK_PAJAK F ON F.KD_PROPINSI||F.KD_DATI2||F.KD_KECAMATAN||F.KD_KELURAHAN||F.KD_BLOK||F.NO_URUT||F.KD_JNS_OP = A.KD_PROPINSI||A.KD_DATI2||A.KD_KECAMATAN||A.KD_KELURAHAN||A.KD_BLOK||A.NO_URUT||A.KD_JNS_OP
			where A.KD_JNS_OP = '" . $KD_JNS_OP . "' AND A.NO_URUT = '" . $NO_URUT . "' AND A.KD_BLOK = '" . $KD_BLOK . "' AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "' AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "' AND A.KD_DATI2 = '" . $KD_DATI2 . "' AND A.KD_PROPINSI ='" . $KD_PROPINSI . "' AND A.THN_PAJAK_SPPT='" . $tahuncari . "'";

        $statement = $this->adapter->query($sql);

        return $statement->execute()->current();
    }

    public function temukanDataTunggakanopInfo($nopcari)
    { //SPPTBase $spt
        //$nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $nop = explode('.', $nopcari);
        $KD_PROPINSI =  $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];
        $sql = "select THN_PAJAK_SPPT, PBB_YG_HARUS_DIBAYAR_SPPT, TO_CHAR(TGL_JATUH_TEMPO_SPPT,'DD-MM-YYYY') AS JATUH_TEMPO from SPPT 
                where KD_JNS_OP = '" . $KD_JNS_OP . "' AND NO_URUT = '" . $NO_URUT . "' AND KD_BLOK = '" . $KD_BLOK . "' AND KD_KELURAHAN = '" . $KD_KELURAHAN . "' AND KD_KECAMATAN = '" . $KD_KECAMATAN . "' AND KD_DATI2 = '" . $KD_DATI2 . "' AND KD_PROPINSI ='" . $KD_PROPINSI . "' AND STATUS_PEMBAYARAN_SPPT='0' AND THN_PAJAK_SPPT>'2013' order by THN_PAJAK_SPPT ASC";

        //KD_JNS_OP||NO_URUT||KD_BLOK||KD_KELURAHAN||KD_KECAMATAN||KD_DATI2||KD_PROPINSI='".$KD_JNS_OP.$NO_URUT.$KD_BLOK.$KD_KELURAHAN.$KD_KECAMATAN.$KD_DATI2.$KD_PROPINSI."' AND STATUS_PEMBAYARAN_SPPT='0'  order by THN_PAJAK_SPPT ASC";
        $statement = $this->adapter->query($sql);

        return $statement->execute();
    }


    public function temukanDataInfoop2(SPPTBase $spt)
    {
        $nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];



        $sql = "select A.*, B.NM_KECAMATAN, C.NM_KELURAHAN, D.NILAI_PER_M2_TANAH, E.NILAI_PER_M2_BNG, F.JALAN_OP, F.BLOK_KAV_NO_OP, F.RW_OP, F.RT_OP
                from SPPT A
                LEFT JOIN REF_KECAMATAN B ON B.KD_PROPINSI = A.KD_PROPINSI AND B.KD_DATI2 = A.KD_DATI2 AND B.KD_KECAMATAN = A.KD_KECAMATAN
                LEFT JOIN REF_KELURAHAN C ON C.KD_PROPINSI = A.KD_PROPINSI AND C.KD_DATI2 = A.KD_DATI2 AND C.KD_KECAMATAN = A.KD_KECAMATAN AND C.KD_KELURAHAN = A.KD_KELURAHAN
                LEFT JOIN KELAS_TANAH D ON D.KD_KLS_TANAH = A.KD_KLS_TANAH
                LEFT JOIN KELAS_BANGUNAN E ON E.KD_KLS_BNG = A.KD_KLS_BNG
                LEFT JOIN DAT_OBJEK_PAJAK F ON F.KD_PROPINSI = A.KD_PROPINSI AND F.KD_DATI2 = A.KD_DATI2 AND F.KD_KECAMATAN = A.KD_KECAMATAN AND F.KD_KELURAHAN = A.KD_KELURAHAN AND F.KD_BLOK = A.KD_BLOK AND F.NO_URUT = A.NO_URUT AND F.KD_JNS_OP = A.KD_JNS_OP
                where 
                    A.KD_PROPINSI = '" . $KD_PROPINSI . "'
                    AND A.KD_DATI2 = '" . $KD_DATI2 . "'
                    AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "' 
                    AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "'
                    AND A.KD_BLOK = '" . $KD_BLOK . "'
                    AND A.NO_URUT = '" . $NO_URUT . "' 
                    AND A.KD_JNS_OP='" . $KD_JNS_OP . "'
                    AND A.THN_PAJAK_SPPT='" . $spt->t_thnspptinfoop . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataTunggakanop(SPPTBase $spt)
    {
        $nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $KD_PROPINSI =  $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];
        $sql = "select A.THN_PAJAK_SPPT, A.PBB_YG_HARUS_DIBAYAR_SPPT, TO_CHAR(A.TGL_JATUH_TEMPO_SPPT,'DD-MM-YYYY') AS JATUH_TEMPO from SPPT A
                where 
                    A.KD_PROPINSI = '" . $KD_PROPINSI . "'
                    AND A.KD_DATI2 = '" . $KD_DATI2 . "'
                    AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "'
                    AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "'
                    AND A.KD_BLOK = '" . $KD_BLOK . "'
                    AND A.NO_URUT = '" . $NO_URUT . "' AND A.KD_JNS_OP='" . $KD_JNS_OP . "'
                    AND A.STATUS_PEMBAYARAN_SPPT='0' AND A.THN_PAJAK_SPPT>=2014 order by A.THN_PAJAK_SPPT ASC";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getGridCount($query, $qtype)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($query) {
            $where->literal("$qtype LIKE '%$query%'");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData($query, $qtype, $start, $rp, $sortname, $sortorder)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($query) {
            $where->literal("$qtype LIKE '%$query%'");
        }
        $select->where($where);
        $select->order($sortname, $sortorder);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getNOP(SPPTBase $ss)
    {
        $sql = "select A.*, B.NM_KECAMATAN, C.NM_KELURAHAN, D.NILAI_PER_M2_TANAH, E.NILAI_PER_M2_BNG, F.NM_DATI2
                from SPPT A
                LEFT JOIN REF_KECAMATAN B ON B.KD_PROPINSI = A.KD_PROPINSI AND B.KD_DATI2 = A.KD_DATI2 AND B.KD_KECAMATAN = A.KD_KECAMATAN
                LEFT JOIN REF_KELURAHAN C ON C.KD_PROPINSI = A.KD_PROPINSI AND C.KD_DATI2 = A.KD_DATI2 AND C.KD_KECAMATAN = A.KD_KECAMATAN AND C.KD_KELURAHAN = A.KD_KELURAHAN
                LEFT JOIN KELAS_TANAH D ON D.KD_KLS_TANAH = A.KD_KLS_TANAH
                LEFT JOIN KELAS_BANGUNAN E ON E.KD_KLS_BNG = A.KD_KLS_BNG
                LEFT JOIN REF_DATI2 F ON F.KD_DATI2 = A.KD_DATI2 
                where A.KD_PROPINSI = '" . $ss->KD_PROPINSI . "'
                        AND A.KD_DATI2 = '" . $ss->KD_DATI2 . "'
                        AND A.KD_KECAMATAN = '" . $ss->KD_KECAMATAN . "'
                        AND A.KD_KELURAHAN = '" . $ss->KD_KELURAHAN . "'
                        AND A.KD_BLOK = '" . $ss->KD_BLOK . "'
                        AND A.NO_URUT = '" . $ss->NO_URUT . "' 
                        AND A.KD_JNS_OP='" . $ss->KD_JNS_OP . "'
                        AND A.THN_PAJAK_SPPT = '" . $ss->THN_PAJAK_SPPT . "'    
                        ";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataInfoopcetak($spt)
    {
        $nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];
        $sql = "select A.*, B.NM_KECAMATAN, C.NM_KELURAHAN, D.NILAI_PER_M2_TANAH, E.NILAI_PER_M2_BNG, F.JALAN_OP, F.BLOK_KAV_NO_OP, F.RW_OP, F.RT_OP
                from SPPT A
                LEFT JOIN REF_KECAMATAN B ON B.KD_PROPINSI = A.KD_PROPINSI AND B.KD_DATI2 = A.KD_DATI2 AND B.KD_KECAMATAN = A.KD_KECAMATAN
                LEFT JOIN REF_KELURAHAN C ON C.KD_PROPINSI = A.KD_PROPINSI AND C.KD_DATI2 = A.KD_DATI2 AND C.KD_KECAMATAN = A.KD_KECAMATAN AND C.KD_KELURAHAN = A.KD_KELURAHAN
                LEFT JOIN KELAS_TANAH D ON D.KD_KLS_TANAH = A.KD_KLS_TANAH
                LEFT JOIN KELAS_BANGUNAN E ON E.KD_KLS_BNG = A.KD_KLS_BNG
                LEFT JOIN DAT_OBJEK_PAJAK F ON F.KD_PROPINSI = A.KD_PROPINSI AND F.KD_DATI2 = A.KD_DATI2 AND F.KD_KECAMATAN = A.KD_KECAMATAN AND F.KD_KELURAHAN = A.KD_KELURAHAN AND F.KD_BLOK = A.KD_BLOK AND F.NO_URUT = A.NO_URUT AND F.KD_JNS_OP = A.KD_JNS_OP
                where A.KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND A.KD_DATI2 = '" . $KD_DATI2 . "'
                        AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "'
                        AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "'
                        AND A.KD_BLOK = '" . $KD_BLOK . "'
                        AND A.NO_URUT = '" . $NO_URUT . "' 
                        AND A.KD_JNS_OP='" . $KD_JNS_OP . "'
                        AND A.THN_PAJAK_SPPT='" . $spt->t_thnspptinfoop . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataTunggakanopcetak($spt)
    {
        $nop = explode('.', $spt->t_nopbphtbspptinfoop);
        $KD_PROPINSI =  $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];
        $sql = "select A.THN_PAJAK_SPPT, A.PBB_YG_HARUS_DIBAYAR_SPPT, TO_CHAR(A.TGL_JATUH_TEMPO_SPPT,'DD-MM-YYYY') AS JATUH_TEMPO from SPPT A
                where A.KD_PROPINSI = '" . $KD_PROPINSI . "'
                        AND A.KD_DATI2 = '" . $KD_DATI2 . "'
                        AND A.KD_KECAMATAN = '" . $KD_KECAMATAN . "'
                        AND A.KD_KELURAHAN = '" . $KD_KELURAHAN . "'
                        AND A.KD_BLOK = '" . $KD_BLOK . "'
                        AND A.NO_URUT = '" . $NO_URUT . "' 
                        AND A.KD_JNS_OP='" . $KD_JNS_OP . "'
                        AND A.STATUS_PEMBAYARAN_SPPT='0' AND A.THN_PAJAK_SPPT>'2013' order by A.THN_PAJAK_SPPT ASC";
        //var_dump($sql);exit();
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataObjek($nop)
    {
        $explode = explode('.', $nop);

        $sql = new Sql($this->adapter);
        $select = $sql->select(['A' => 'DAT_OBJEK_PAJAK']);
        $where = new Where();
        $where->equalTo('A.KD_PROPINSI', $explode[0]);
        $where->equalTo('A.KD_DATI2', $explode[1]);
        $where->equalTo('A.KD_KECAMATAN', $explode[2]);
        $where->equalTo('A.KD_KELURAHAN', $explode[3]);
        $where->equalTo('A.KD_BLOK', $explode[4]);
        $where->equalTo('A.NO_URUT', $explode[5]);
        $where->equalTo('A.KD_JNS_OP', $explode[6]);
        $select->where($where);
        // die($sql->buildSqlString($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getDataObjekSubjek($subjekPajakId)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(['A' => 'DAT_SUBJEK_PAJAK']);
        $select->columns([
            '*',
            'SUBJEK_PAJAK_ID' => new Expression("REPLACE(A.SUBJEK_PAJAK_ID, ' ', '')")
        ]);
        $where = new Where();
        $where->literal("REPLACE(A.SUBJEK_PAJAK_ID, ' ', '') = '" . str_ireplace(' ', '', $subjekPajakId) . "'");
        $select->where($where);
        // die($sql->buildSqlString($select));

        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getDataObjekBumi($nop)
    {
        $explode = explode('.', $nop);

        $sql = new Sql($this->adapter);
        $select = $sql->select(['A' => 'DAT_OP_BUMI']);
        $where = new Where();
        $where->equalTo('A.KD_PROPINSI', $explode[0]);
        $where->equalTo('A.KD_DATI2', $explode[1]);
        $where->equalTo('A.KD_KECAMATAN', $explode[2]);
        $where->equalTo('A.KD_KELURAHAN', $explode[3]);
        $where->equalTo('A.KD_BLOK', $explode[4]);
        $where->equalTo('A.NO_URUT', $explode[5]);
        $where->equalTo('A.KD_JNS_OP', $explode[6]);
        $select->where($where);

        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getDataObjekBangunan($nop)
    {
        $explode = explode('.', $nop);

        $sql = new Sql($this->adapter);
        $select = $sql->select(['A' => 'DAT_OP_BANGUNAN']);
        $select->columns([
            "*",
            "DAYA_LISTRIK" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::LISTRIK . "'
            )"),
            "AC_SPLIT" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_SPLIT . "'
            )"),
            "AC_WINDOW" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_WINDOW . "'
            )"),
            "AC_CENTRAL_KANTOR" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_CENTRAL_KANTOR . "'
            )"),
            "AC_CENTRAL_KAMAR_HOTEL" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_CENTRAL_KAMAR_HOTEL . "'
            )"),
            "AC_CENTRAL_PERTOKOAN" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_CENTRAL_PERTOKOAN . "'
            )"),
            "AC_CENTRAL_KAMAR_RUMAH_SAKIT" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_CENTRAL_KAMAR_RUMAH_SAKIT . "'
            )"),
            "AC_CENTRAL_APARTEMEN" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_CENTRAL_APARTEMEN . "'
            )"),
            "AC_CENTRAL_BANGUNAN_LAIN" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::AC_CENTRAL_BANGUNAN_LAIN . "'
            )"),
            "KOLAM_RENANG_DIPLESTER" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::KOLAM_RENANG_DIPLESTER . "'
            )"),
            "KOLAM_RENANG_DENGAN_PELAPIS" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::KOLAM_RENANG_DENGAN_PELAPIS . "'
            )"),
            "PERKERASAN_KONSTRUKSI_RINGAN" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::PERKERASAN_KONSTRUKSI_RINGAN . "'
            )"),
            "PERKERASAN_KONSTRUKSI_SEDANG" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::PERKERASAN_KONSTRUKSI_SEDANG . "'
            )"),
            "PERKERASAN_KONSTRUKSI_BERAT" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::PERKERASAN_KONSTRUKSI_BERAT . "'
            )"),
            "PAGAR_BAJA_BESI" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::PAGAR_BAJA_BESI . "'
            )"),
            "PAGAR_BATA_BATAKO" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::PAGAR_BATA_BATAKO . "'
            )"),
            "GENSET" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::GENSET . "'
            )"),
            "PABX" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::PABX . "'
            )"),
            "SUMUR_ARTESIS" => new Expression("(
                SELECT AA.JML_SATUAN FROM DAT_FASILITAS_BANGUNAN AA 
                WHERE AA.KD_PROPINSI = A.KD_PROPINSI AND AA.KD_DATI2 = A.KD_DATI2 
                AND AA.KD_KECAMATAN = A.KD_KECAMATAN AND AA.KD_KELURAHAN = A.KD_KELURAHAN 
                AND AA.KD_BLOK = A.KD_KELURAHAN AND AA.NO_URUT = A.NO_URUT 
                AND AA.KD_JNS_OP = A.KD_JNS_OP AND AA.NO_BNG = A.NO_BNG 
                AND AA.KD_FASILITAS = '" . ReffSpopHelper::SUMUR_ARTESIS . "'
            )"),
        ]);
        $where = new Where();
        $where->equalTo('A.KD_PROPINSI', $explode[0]);
        $where->equalTo('A.KD_DATI2', $explode[1]);
        $where->equalTo('A.KD_KECAMATAN', $explode[2]);
        $where->equalTo('A.KD_KELURAHAN', $explode[3]);
        $where->equalTo('A.KD_BLOK', $explode[4]);
        $where->equalTo('A.NO_URUT', $explode[5]);
        $where->equalTo('A.KD_JNS_OP', $explode[6]);
        $select->where($where);
        $select->order("A.NO_BNG");
        // die($sql->buildSqlString($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        //->current();
        // return $res;
        $resultSet = new ResultSet();
        $resultSet->initialize($res);
        return $resultSet->toArray();
    }

    public function getDataZnt($kd_kecamatan, $kd_kelurahan, $kd_blok, $tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(['A' => 'DAT_PETA_ZNT']);
        $select->columns([
            "KD_ZNT",
            "NILAI_PER_METER" => new Expression("(SELECT X.NILAI_PER_M2_TANAH * 1000 
            FROM KELAS_TANAH X 
            WHERE X.THN_AWAL_KLS_TANAH <= B.THN_NIR_ZNT AND X.THN_AKHIR_KLS_TANAH >= B.THN_NIR_ZNT 
            AND B.NIR BETWEEN X.NILAI_MIN_TANAH AND X.NILAI_MAX_TANAH AND X.NILAI_PER_M2_TANAH > 0 
            AND ROWNUM = 1)")
        ]);
        $select->join(['B' => 'DAT_NIR'], "A.KD_PROPINSI = B.KD_PROPINSI AND A.KD_DATI2 = B.KD_DATI2 
        AND A.KD_KECAMATAN = B.KD_KECAMATAN AND A.KD_KELURAHAN = B.KD_KELURAHAN 
        AND A.KD_ZNT = B.KD_ZNT", [], "LEFT");
        $where = new Where();
        $where->equalTo("A.KD_KECAMATAN", $kd_kecamatan);
        $where->equalTo("A.KD_KELURAHAN", $kd_kelurahan);
        $where->equalTo("A.KD_BLOK", $kd_blok);
        $where->equalTo("B.THN_NIR_ZNT", $tahun);

        $select->where($where);

        $res = $sql->prepareStatementForSqlObject($select)->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($res);
        return $resultSet->toArray();
    }
}
