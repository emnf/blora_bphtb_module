<?php

//modul pembayaran

namespace Bphtb\Model\Pembayaran;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class PembayaranSptTable extends AbstractTableGateway
{

    protected $table = 't_pembayaranspt', $tableview = 'view_sspd_semua_pembayaran';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PembayaranSptBase());
        $this->initialize();
    }

    //================= datagrid pembayaran
    public function getjumlahdata($sTable, $count, $sWhere)
    {
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    public function semuadatapembayaran($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }

                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NOT NULL";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NOT NULL";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }



            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/tervalidasi.png">';
            } else {
                $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/belumdivalidasi.png">';
            }



            if ($aRow['t_ketetapanspt'] == 1) {
                $validasipembayaran = '<a href="pembayaran_sptbphtb/cetakbuktivalidasipembayaran?&action=cetakbuktivalidasipembayaran&t_idspt=' . $aRow['t_idspt'] . '" target="_blank"><i class="fa fa-fw fa-print"></i> Validasi Pembayaran</a>';
                $buktipembayaran = '<a href="pembayaran_sptbphtb/cetakbuktipembayaran?&action=cetakbuktipembayaran&t_idspt=' . $aRow['t_idspt'] . '" target="_blank"><i class="fa fa-fw fa-print"></i> Bukti Pembayaran</a>';
                $lihat = '<a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';
                $hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . '); return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            } else {
                $validasipembayaran = ' ';
                $buktipembayaran = ' ';
                $lihat = ' ';
                $hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');updatepemeriksaan(' . $aRow['p_idpemeriksaan'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            }

            /*$btn = '<div class="btn-group">
                            <button aria-expanded="false" type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown">
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" style="left:-120px; border-color: blue;">
                                 '.$validasipembayaran.'
                                 '.$buktipembayaran.'    
                                 '.$lihat.'
                                 '.$hapus.'   
                            </ul>
                          </div>';*/

            $btn = '<div class="dropdown">
                    <button onclick="myFunction(' . $aRow['t_idspt'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                                    ' . $validasipembayaran . '
                                 ' . $buktipembayaran . '    
                                 ' . $lihat . '
                                 ' . $hapus . '     
                    </div>
                  </div>';



            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $t_kohirspt . "</center>",
                "<center>" . $novalidasi . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglverifikasispt'])) . "</span>",
                $aRow['t_namawppembeli'],
                "<span style='float:right;'>" . number_format($aRow['t_nilaipembayaranspt'], 0, ',', '.') . "</span>",
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                "<center>" . $status_bayar . "</center>",
                "<center>" . $status_verifikasi . "</center>",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function semuadatatervalidasi($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    if ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NULL AND t_kodebayarbanksppt IS NOT NULL AND status_validasi = 1";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NULL AND t_kodebayarbanksppt IS NOT NULL AND status_validasi = 1";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 


        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        //var_dump($sql);
        //exit();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            /*
            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="'.$cekurl.'/public/img/syaratlengkap.png">';
            }else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="'.$cekurl.'/public/img/syarattidaklengkap.png">';
            }
            
            
            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            }else {
                $status_bayar = '<a class="btn btn-danger btn-sm" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }
            
            if(($session['s_namauserrole'] == "Administrator")){
                $admin_hapus = '<li style="background-color:red;"><a style="color:#fff;" href="#" onclick="hapusall('. $aRow['t_idspt'].');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus Semua</a></li>';
            }else{
                $admin_hapus = '';
            }
            
            
            
            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $batal = '';
                $cetaksurat = '';
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="'.$cekurl.'/public/img/tervalidasi.png">';
                if ($aRow['t_statusbayarspt'] == 'TRUE') {
                    $edit = '';
                } else {
                    $edit = '<li><a href="verifikasi_spt/edit?t_idpembayaranspt='. $aRow['t_idpembayaranspt'] .'"><i class="fa fa-fw fa-edit"></i> Edit</a></li>';
                }
                
            }else {
                $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="'.$cekurl.'/public/img/belumdivalidasi.png">';
                    
                $cetaksurat = '<a href="#" onclick="openCetakBukti('. $aRow['t_kohirspt'].');return false;">Surat Pemberitahuan</a>';
                
                
                if ($aRow['t_inputbpn'] == true) {
                    $edit = '<li><a href="verifikasi_spt/edit?t_idpembayaranspt='. $aRow['t_idpembayaranspt'].'"><i class="fa fa-fw fa-edit"></i> Edit</a></li>';
                    $batal = '';
                } else {
                    $edit = '<li><a href="verifikasi_spt/edit?t_idpembayaranspt='. $aRow['t_idpembayaranspt'].'"><i class="fa fa-fw fa-edit"></i> Edit</a></li>';
                    $batal = '<li><a href="#" onclick="hapus('.$aRow['t_idpembayaranspt'].');return false;">Batal</a></li>';
                }
            }
            
            if(!empty($aRow['p_idpemeriksaan'])) {
                $jmlpajak = "<span style='float:right;color:blue;'>".number_format($aRow['p_totalspt'], 0, ',', '.')."</span>";
                $cetaksuratpenelitian = '<li><a href="#" onclick="openCetakPenelitian('. $aRow['t_kohirspt'].');return false;"><i class="fa fa-fw fa-print"></i> Surat Penelitian</a></li>';
            
            }else {
                $jmlpajak = "<span style='float:right;'>".number_format($aRow['t_totalspt'], 0, ',', '.')."</span>";
                $cetaksuratpenelitian = '';
            }  
            
            */

            $btn = '<a href="#" class="btn btn-block btn-info btn-sm" onclick="pilihPendataanSspdBphtb(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-hand-pointer-o"></i> PILIH</a>';

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;">' . $aRow['t_kohirspt'] . '</span>';
            } else {
                $t_kohirspt = $aRow['t_kohirspt'];
            }




            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $t_kohirspt . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",

                "<center>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</center>",
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                $aRow['t_namawppembeli'],

                $aRow['t_namawppenjual'],
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",

                "<span style='float:right;'>" . number_format($aRow['jml_pajak_v1'], 0, ',', '.') . "</span>",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }


    public function semuadatabpn($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }

                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NOT NULL AND t_inputbpn IS NULL ";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NOT NULL AND t_inputbpn IS NULL";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '(<span style="color: green;"><b> <i class="fa fa-fw fa-money"></i> SUDAH </b></span>)';
            } else {
                $status_bayar = '<a class="btn btn-danger btn-sm" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }



            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $status_verifikasi = '( <span style="color: green;"><b><i class="fa fa-fw fa-check-circle-o"></i> Tervalidasi</b></span> )';
            } else {
                $status_verifikasi = '( Belum Tervalidasi )';
            }



            if ($aRow['t_ketetapanspt'] == 1) {
                $validasipembayaran = '<li><a href="pembayaran_sptbphtb/cetakbuktivalidasipembayaran?&action=cetakbuktivalidasipembayaran&t_idspt=' . $aRow['t_idspt'] . '" target="_blank"><i class="fa fa-fw fa-print"></i> Validasi Pembayaran</a></li>';
                $buktipembayaran = '<li><a href="pembayaran_sptbphtb/cetakbuktipembayaran?&action=cetakbuktipembayaran&t_idspt=' . $aRow['t_idspt'] . '" target="_blank"><i class="fa fa-fw fa-print"></i> Bukti Pembayaran</a></li>';
                $lihat = '<li><a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a></li>';
                $hapus = '<li style="background-color:red;"><a style="color:#fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . '); return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a></li>';
            } else {
                $validasipembayaran = ' ';
                $buktipembayaran = ' ';
                $lihat = ' ';
                $hapus = '<li style="background-color:red;"><a style="color:#fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');updatepemeriksaan(' . $aRow['p_idpemeriksaan'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a></li>';
            }

            $btn = "<form method='post' action='bpn_lihatdata/edit' id='formtambah'><input type='hidden' name='t_idspt' value='" . $aRow['t_iddetailsptbphtb'] . "'/> <input type='hidden' name='status' value='1'/><input value='Cek Detail' class='btn btn-primary btn-sm btn-flat' type='submit'>  </form>";

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;">' . $aRow['t_kohirspt'] . '</span>';
            } else {
                $t_kohirspt = $aRow['t_kohirspt'];
            }

            if (!empty($aRow['p_idpemeriksaan'])) {
                $luastanah = number_format($aRow['p_luastanah'], 0, ',', '.');
                $luasbangungan = number_format($aRow['p_luasbangunan'], 0, ',', '.');
            } else {
                $luastanah = number_format($aRow['t_luastanah'], 0, ',', '.');
                $luasbangungan = number_format($aRow['t_luasbangunan'], 0, ',', '.');
            }

            if (empty($aRow['t_tglsertifikatbaru'])) {
                $nosertifikat = '-';
            } else {
                $nosertifikat = $aRow['t_nosertifikatbaru'] . " / " . date('d-m-Y', strtotime($aRow['t_tglsertifikatbaru']));
            }

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $t_kohirspt . "</center>",
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tglverifikasispt'])) . "<br>" . $status_verifikasi . "</center>",
                $aRow['t_namawppembeli'],
                "<span style='float:right;'>" . number_format($aRow['t_nilaipembayaranspt'], 0, ',', '.') . "</span><br><center>" . $status_bayar . "</center>",


                "<center>" . $luastanah . " / " . $luasbangungan . "</center>",
                "<center>" . $aRow['fr_luas_tanah_bpn'] . " / " . $aRow['fr_luas_bangunan_bpn'] . "</center>",

                "<center>" . $nosertifikat . "</center>",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }


    public function semuadatakpppratama($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }

                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NOT NULL";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NOT NULL";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            $aphb_kali = $aRow['t_tarif_pembagian_aphb_kali'];
            $aphb_bagi = $aRow['t_tarif_pembagian_aphb_bagi'];

            if (!empty($aRow['p_idpemeriksaan'])) {

                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($aRow['p_grandtotalnjop'] > $aRow['p_nilaitransaksispt']) {
                        $npop = $aRow['p_grandtotalnjop'];
                    } else {
                        $npop = $aRow['p_nilaitransaksispt'];
                    }
                } else {
                    if ($aRow['p_grandtotalnjop_aphb'] > $aRow['p_nilaitransaksispt']) {
                        $npop = $aRow['p_grandtotalnjop_aphb'];
                    } else {
                        $npop = $aRow['p_nilaitransaksispt'];
                    }
                }
            } else {
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($aRow['t_grandtotalnjop'] > $aRow['t_nilaitransaksispt']) {
                        $npop = $aRow['t_grandtotalnjop'];
                    } else {
                        $npop = $aRow['t_nilaitransaksispt'];
                    }
                } else {
                    if ($aRow['t_grandtotalnjop_aphb'] > $aRow['t_nilaitransaksispt']) {
                        $npop = $aRow['t_grandtotalnjop_aphb'];
                    } else {
                        $npop = $aRow['t_nilaitransaksispt'];
                    }
                }
            }

            $aksi =  '<a href="kpppratama/viewdata?t_idspt=' . $aRow['t_idspt'] . '" class="btn btn-primary btn-md">Detail</a>';
            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_kohirspt'] . "</center>",
                "" . $aRow['s_namanotaris'] . "",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tanggalpembayaran'])) . "</center>",
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                $aRow['t_namawppembeli'],
                "" . $aRow['t_alamatwppembeli'] . "",
                "<center>" . $aRow['t_nikwppembeli'] . "</center>",
                "<span style='float:right;'>" . number_format($npop, 0, ',', '.') . "</span>",
                "<center>" . $aksi . "</center>",

            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }


    //================= end datagrid pembayaran

    public function temukanDataSspd(PembayaranSptBase $spt)
    {
        $sql = "select * from view_sspd_pembayaran where t_kohirspt::text='" . $spt->t_kohirpembayaran . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataSkpdkb(PembayaranSptBase $spt)
    {
        $sql = "select * from view_sspd_validasi where p_kohirskpdkb::text='" . $spt->p_kohirskpdkb . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function cekJumlahhari()
    {
        $sql = "select * from s_tempo";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function cekValiditasSSPD(PembayaranSptBase $spt, $jumlahhari)
    {
        $sql = "select * from view_sspd where t_kohirspt=" . $spt->t_kohirpembayaran . " and t_tglprosesspt <= '" . date('Y-m-d', strtotime($spt->t_tanggalpembayaran)) . "' and (t_tglprosesspt + interval '$jumlahhari days') >= '" . date('Y-m-d', strtotime($spt->t_tanggalpembayaran)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    //Simpan Pembayaran SSPD / SKPDKB
    //Lokasi : Tambah Pembayaran
    public function savedataPembayaran(PembayaranSptBase $pembspt, $iduser)
    {
        $data = array(
            't_idspt' => $pembspt->t_idspt,
            't_periodepembayaran' => date('Y', strtotime($pembspt->t_tanggalpembayaran)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($pembspt->t_tanggalpembayaran)),
            't_nilaipembayaranspt' => str_ireplace(".", "", $pembspt->t_nilaipembayaranspt),
            't_statusbayarspt' => true,
            't_idpenerimasetoran' => $iduser
        );
        $this->insert($data);
    }

    public function updatedatapembayaranTrue(PembayaranSptBase $pembspt, $iduser)
    {
        $data = array(
            't_statusbayarspt' => TRUE,
            't_periodepembayaran' => date('Y', strtotime($pembspt->t_tanggalpembayaran)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($pembspt->t_tanggalpembayaran)),
            't_ketetapanspt' => 1,
            't_nilaipembayaranspt' => str_ireplace(".", "", $pembspt->t_nilaipembayaranspt),
            't_idpenerimasetoran' => $iduser
        );
        $t_pembayaranspt = new \Zend\Db\TableGateway\TableGateway('t_pembayaranspt', $this->adapter);
        $t_pembayaranspt->update($data, array('t_idpembayaranspt' => $pembspt->t_idpembayaranspt));

        //echo \Zend\Debug\Debug::dump($t_pembayaranspt, $label = null, $echo = false); //$t_pembayaranspt->getSqlString();

    }

    public function updatedatapemeriksaanTrue(PembayaranSptBase $pembspt)
    {
        $data = array(
            'p_pembayaranskpdkb' => TRUE
        );
        $t_pemeriksaan = new \Zend\Db\TableGateway\TableGateway('t_pemeriksaan', $this->adapter);
        $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $pembspt->p_idpemeriksaan));
    }

    public function updatedatapemeriksaanFalse(PembayaranSptBase $pembspt)
    {
        $data = array(
            'p_pembayaranskpdkb' => FALSE
        );
        $t_pemeriksaan = new \Zend\Db\TableGateway\TableGateway('t_pemeriksaan', $this->adapter);
        $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $pembspt->p_idpemeriksaan));
    }

    //Simpan Pembayaran SSPD / SKPDKB
    //Lokasi : Tambah Pembayaran
    public function savedataPembayaranDenda(PembayaranSptBase $pembspt)
    {
        $data = array(
            't_idds' => $pembspt->t_idds,
            't_periodepembayaran' => date('Y', strtotime($pembspt->t_tanggalpembayaran)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($pembspt->t_tanggalpembayaran)),
            't_nilaipembayaranspt' => str_ireplace(".", "", $pembspt->t_jumlahds),
            't_kodebayarbanksppt' => $pembspt->t_kodebayards,
            't_statusbayarspt' => true
        );
        $this->insert($data);
    }

    //jumlah SSPD dan SKPDKB yang dibayarkan
    //Lokasi : Index Pembayaran
    public function getGridCount(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        if ($base->t_nilaipembayaranspt != 'undefined')
            $where->literal("t_nilaipembayaranspt::text LIKE '%$base->t_nilaipembayaranspt%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    //Data SSPD dan SKPDKB yang dibayarkan
    //Lokasi : Index Pembayaran
    public function getGridData(PembayaranSptBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        if ($base->t_nilaipembayaranspt != 'undefined')
            $where->literal("t_nilaipembayaranspt::text LIKE '%$base->t_nilaipembayaranspt%'");
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->order("t_tanggalpembayaran DESC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getRealisasiAnggaran()
    {
        $sql = "select sum(t_nilaipembayaranspt) from t_pembayaranspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    //Jumlah Data Verifikasi
    //Lokasi : Index Verifikasi
    public function getGridCountVerifikasi(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    //Data Verifikasi
    //Lokasi : Index Verifikasi
    public function getGridDataVerifikasi(PembayaranSptBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id)
    {
        // $rowset = $this->select(function (Select $select) use ($id) {
        //     $select->where(array('t_pembayaranspt.t_idpembayaranspt' => $id));
        //     $select->join('view_sspd', 't_pembayaranspt.t_idspt = view_sspd.t_idspt');
        //     $select->join('view_sspd_semua_pembayaran', 'view_sspd.t_idspt = view_sspd_semua_pembayaran.t_idspt');
        // });
        // $row = $rowset->current();
        // return $row;
        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "t_pembayaranspt"])->where(["a.t_idpembayaranspt" => $id]);
        $select->join(["b" => "t_spt"], "a.t_idspt = b.t_idspt", [
            "t_idjenistransaksi", "t_nopbphtbsppt",
            "t_nilaitransaksispt"
        ], "LEFT");
        $select->join(["c" => "t_detailsptbphtb"], "a.t_idspt = c.t_idspt", [
            "t_totalnjopbangunan", "t_luastanah"
        ], "LEFT");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getDataIdEdit($t_idpembayaran)
    {


        /*$sql = new \Zend\Db\Sql\Sql ( $this->adapter );
		$select = $sql->select ();
		$select->from ( array (
				"a" => $this->tableview
		) );
                $where = new \Zend\Db\Sql\Where ();
		$where->equalTo ( "t_idpembayaranspt", $t_idpembayaran );
                $select->where ( $where );
		$state = $sql->prepareStatementForSqlObject ( $select );
		$res = $state->execute ();
		return $res->current ();*/
        $sql = "select * from view_sspd_semua_pembayaran where t_idpembayaranspt=$t_idpembayaran";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataPemeriksaanId($t_idpembayaran)
    {
        //$sql = "select * from t_pemeriksaan where p_idpembayaranspt=$t_idpembayaran";
        // $sql = "select a.*,c.t_tarif_pembagian_aphb_kali, c.t_tarif_pembagian_aphb_bagi from t_pemeriksaan a  
        //             LEFT JOIN t_pembayaranspt b ON a.p_idpembayaranspt = b.t_idpembayaranspt
        //             left join t_spt c on b.t_idspt = c.t_idspt
        //         where a.p_idpembayaranspt=$t_idpembayaran";
        // $statement = $this->adapter->query($sql);
        // return $statement->execute()->current();
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => "t_pemeriksaan"]);
        $select->join(["b" => "t_pembayaranspt"], new Expression("a.p_idpembayaranspt = b.t_idpembayaranspt"), [], "LEFT");
        $select->join(["c" => "t_spt"], new Expression("b.t_idspt = c.t_idspt"), [
            "t_tarif_pembagian_aphb_kali", "t_tarif_pembagian_aphb_bagi"
        ], "LEFT");
        $where = new Where();
        $where->literal("a.p_idpembayaranspt=$t_idpembayaran");
        $select->where($where);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    //Hapus Data Pembayaran
    //Lokasi : Index Pembayaran
    public function hapusData(PembayaranSptBase $kb)
    {
        $dp = $this->getDataId($kb->t_idpembayaranspt);
        // if ($dp->t_verifikasispt) {
        //     return false;
        // } else {
        //     $this->delete(array('t_idpembayaranspt' => $kb->t_idpembayaranspt));
        //     return true;
        // }
    }

    public function getDataPembayaran($t_idspt)
    {
        $sql = "select * from t_pembayaranspt where t_idspt=$t_idspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getGridCountBpn(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $select->where("t_tanggalpembayaran is not null");
        $where->equalTo('t_ketetapanspt', '1');
        $where->isNull('t_inputbpn');
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_luastanah != 'undefined')
            $where->literal("t_luastanah::text LIKE '%$base->t_luastanah%'");
        if ($base->t_luasbangunan != 'undefined')
            $where->literal("t_luasbangunan::text LIKE '%$base->t_luasbangunan%'");
        if ($base->t_luastanahbpn != 'undefined')
            $where->literal("t_luastanahbpn::text LIKE '%$base->t_luastanahbpn%'");
        if ($base->t_luasbangunanbpn != 'undefined')
            $where->literal("t_luasbangunanbpn::text LIKE '%$base->t_luasbangunanbpn%'");
        if ($base->t_nosertifikatbaru != 'undefined')
            $where->literal("t_nosertifikatbaru::text LIKE '%$base->t_nosertifikatbaru%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataBpn(PembayaranSptBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $select->where("t_tanggalpembayaran is not null");
        $where->equalTo('t_ketetapanspt', '1');
        $where->isNull('t_inputbpn');
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_luastanah != 'undefined')
            $where->literal("t_luastanah::text LIKE '%$base->t_luastanah%'");
        if ($base->t_luasbangunan != 'undefined')
            $where->literal("t_luasbangunan::text LIKE '%$base->t_luasbangunan%'");
        if ($base->t_luastanahbpn != 'undefined')
            $where->literal("t_luastanahbpn::text LIKE '%$base->t_luastanahbpn%'");
        if ($base->t_luasbangunanbpn != 'undefined')
            $where->literal("t_luasbangunanbpn::text LIKE '%$base->t_luasbangunanbpn%'");
        if ($base->t_nosertifikatbaru != 'undefined')
            $where->literal("t_nosertifikatbaru::text LIKE '%$base->t_nosertifikatbaru%'");
        $select->where($where);
        $select->order("t_tanggalpembayaran DESC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    //jumlah SSPD dan SKPDKB yang dibayarkan
    //Lokasi : Index Pembayaran
    public function getJumlahPembayaran(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahVerifikasi(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahVerifikasBerkasilogin($idlogin)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        // $where->isNotNull('t_tglverifikasispt');
        $where->isNotNull('t_verifikasiberkas');
        //$where->literal("t_idnotarisspt = ".$idlogin."");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahVerifikasilogin($idlogin)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $where->literal("t_idnotarisspt = " . $idlogin . "");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahPembayaranlogin($idlogin)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $where->literal("t_idnotarisspt = " . $idlogin . "");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahPembayaranlapor($role, $idlogin)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $where->isNotNull("t_noajbbaru");
        if ($role == 3) {
            $where->literal("t_idnotarisspt = " . $idlogin . "");
        } else {
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getDataByKodeBayar($kodeBayar)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(['a' => 't_spt']);
        $select->columns([
            't_idspt',
            't_kohirspt', 't_tglprosesspt', 't_periodespt', 't_nopbphtbsppt'
        ]);
        $select->join(['b' => 't_detailsptbphtb'], "a.t_idspt = b.t_idspt", [
            't_namawppembeli',    't_nikwppembeli', 't_alamatwppembeli', 't_kecamatanwppembeli', 't_kelurahanwppembeli', 't_kabkotawppembeli', 't_telponwppembeli', 't_kodeposwppembeli', 't_npwpwppembeli', 't_luastanah', 't_njoptanah', 't_luasbangunan', 't_njopbangunan', 't_totalnjoptanah', 't_totalnjopbangunan', 't_grandtotalnjop', 't_nosertifikathaktanah', 't_kelurahanop', 't_kecamatanop', 't_rtwppembeli', 't_rwwppembeli', 't_alamatop', 't_rtop', 't_rwop', 't_namasppt', 't_kabupatenop', 't_nosertifikatbaru', 't_tglsertifikatbaru', 't_luastanah_sismiop', 't_luasbangunan_sismiop', 't_njoptanah_sismiop', 't_njopbangunan_sismiop', 't_grandtotalnjop_aphb',
        ], 'LEFT');
        $select->join(['c' => $this->table], "a.t_idspt = c.t_idspt", [
            't_statusbayarspt', 't_tanggalpembayaran', 't_nilaipembayaranspt', 't_kodebayarbanksppt'
        ], 'LEFT');
        $select->join(['d' => 't_pemeriksaan'], "c.t_idpembayaranspt = d.p_idpembayaranspt", [
            'p_idpemeriksaan', 'p_luastanah', 'p_luasbangunan', 'p_njoptanah', 'p_njopbangunan', 'p_totalnjoptanah', 'p_totalnjopbangunan', 'p_grandtotalnjop', 'p_nilaitransaksispt', 'p_potonganspt', 'p_idjenistransaksi', 'p_idjenishaktanah', 'p_totalspt', 'p_nilaipembayaranspt', 'p_grandtotalnjop_aphb',
            'luas_tanah_fix' => new Expression("(CASE WHEN d.p_idpemeriksaan IS NOT NULL THEN d.p_luastanah ELSE b.t_luastanah END)")
        ], 'LEFT');
        $where = new Where();
        $where->equalTo('c.t_kodebayarbanksppt', $kodeBayar);
        $select->where($where);
        // die($sql->buildSqlString($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }
}
