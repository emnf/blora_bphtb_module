<?php

namespace Bphtb\Model\BpnApi;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class DataSertifikatTable extends AbstractTableGateway
{

    protected $table = 't_datasertifikat_bpn';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DataSertifikatBase());
        $this->initialize();
    }

    public function getUserBpn()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('bpn_users')->where('s_id = 1');
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    // public function cekidkatabpn($data_get)
    // {
    //     $sql = new Sql($this->adapter);
    //     $select = $sql->select();
    //     $select->from($this->table);
    //     $where = new Where();
    //     $where->literal("aktaid = '" . $data_get['AKTAID'] . "' ");
    //     $select->where($where);
    //     $state = $sql->prepareStatementForSqlObject($select);
    //     $res = $state->execute()->current();
    //     return $res;
    // }

    public function cekNoTglAkta($data_get)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        $where->literal("no_akta = '" . $data_get['NOMOR_AKTA'] . "'");
        $where->literal("ppat = '" . $data_get['NAMA_PPAT'] . "'");
        $where->literal("tgl_akta = '" . date('Y-m-d', strtotime(str_ireplace("/", "-", $data_get['TANGGAL_AKTA']))) . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function simpandatasertifikatbpn($tgltransaksi, $row, $session)
    {

        // $cekaktaid = $this->cekidkatabpn($row);

        $data = array(
            // 'aktaid' => $row['AKTAID'],
            // 'tgl_akta' => date('Y-m-d', strtotime(str_ireplace("/", "-", $row['TGL_AKTA']))),
            'tgl_akta' => date('Y-m-d', strtotime(str_ireplace("/", "-", $row['TANGGAL_AKTA']))),
            'nop' => $row['NOP'],
            // 'nib' => $row['NIB'],
            'nib' => $row['NOMOR_INDUK_BIDANG'],
            'nik' => $row['NIK'],
            'npwp' => $row['NIK'],
            'nama_wp' => $row['NAMA_WP'],
            // 'alamat_op' => $row['ALAMAT_OP'],
            'kelurahan_op' => $row['KELURAHAN_OP'],
            'kecamatan_op' => $row['KECAMATAN_OP'],
            'kota_op' => $row['KOTA_OP'],
            'luastanah_op' => $row['LUASTANAH_OP'],
            // 'luasbangunan_op' => $row['LUASBANGUNAN_OP'],
            // 'ppat' => $row['PPAT'],
            'ppat' => $row['NAMA_PPAT'],
            // 'no_sertipikat' => $row['NO_SERTIPIKAT'],
            // 'no_akta' => $row['NO_AKTA'],
            'no_akta' => $row['NOMOR_AKTA'],
            'tgl_system' => date('Y-m-d'),
            't_iduser' => $session['s_iduser'],
            't_tgltransaksi' => date('Y-m-d', strtotime(str_ireplace("/", "-", $tgltransaksi))),
            'ntpd' => $row["NTPD"],
            'koordinat_x' => $row["KOORDINAT_X"],
            'koordinat_y' => $row["KOORDINAT_Y"],
            'jenis_hak' => $row["JENIS_HAK"],

        );

        // if (!empty($cekaktaid['aktaid'])) {
        //     $simpan = $this->update($data, array('aktaid' => $row['AKTAID']));
        // } else {
        //     $simpan = $this->insert($data);
        // }

        $cekNoTglAkta = $this->cekNoTglAkta($row);
        if ($cekNoTglAkta) {
            $simpan = $this->update($data, [
                "no_akta = '" . $row['NOMOR_AKTA'] . "'",
                "tgl_akta = '" . date('Y-m-d', strtotime(str_ireplace("/", "-", $row['TANGGAL_AKTA']))) . "'",
                "ppat = '" . $row['NAMA_PPAT'] . "'"
            ]);
        } else {
            $simpan = $this->insert($data);
        }

        return $simpan;
    }

    public function semuadatasertifikatbpn($input, $aColumns, $session, $cekurl, $allParams)
    {
        $aOrderingRules = array();
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_datasertifikat_bpn");
        $select->join("s_users", "s_users.s_iduser = t_datasertifikat_bpn.t_iduser", array(), "left");
        $select->join("s_pejabat", new \Zend\Db\Sql\Expression("s_pejabat.s_idpejabat::int = s_users.s_idpejabat_idnotaris::int"), array("idpejabatttd_sspd" => "s_idpejabat"), "left");

        $where = new \Zend\Db\Sql\Where();
        if (($input->getPost('sSearch_1')) || ($input->getPost('sSearch_1') == '0')) {
            $tanggal = explode('-', $input->getPost('sSearch_1'));
            if (count($tanggal) > 1) {
                if (count($tanggal) > 2) {
                    $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("t_tgltransaksi::text ILIKE '%" . $tanggalcari . "%'");
                } else {
                    $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("t_tgltransaksi::text ILIKE '%" . $tanggalcari . "%'");
                }
            } else {
                $where->literal("t_tgltransaksi::text ILIKE '%" . $input->getPost('sSearch_1') . "%'");
            }
        }

        if (($input->getPost('sSearch_2')) || ($input->getPost('sSearch_2') == '0')) {
            $where->literal("aktaid::text ILIKE '%" . $input->getPost('sSearch_2') . "%'");
        }

        if (($input->getPost('sSearch_3')) || ($input->getPost('sSearch_3') == '0')) {
            $tanggal = explode('-', $input->getPost('sSearch_3'));
            if (count($tanggal) > 1) {
                if (count($tanggal) > 2) {
                    $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("tgl_akta::text ILIKE '%" . $tanggalcari . "%'");
                } else {
                    $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("tgl_akta::text ILIKE '%" . $tanggalcari . "%'");
                }
            } else {
                $where->literal("tgl_akta::text ILIKE '%" . $input->getPost('sSearch_3') . "%'");
            }
        }

        if (($input->getPost('sSearch_4')) || ($input->getPost('sSearch_4') == '0')) {
            $where->literal("nop::text ILIKE '%" . $input->getPost('sSearch_4') . "%'");
        }

        if (($input->getPost('sSearch_5')) || ($input->getPost('sSearch_5') == '0')) {
            $where->literal("nib::text ILIKE '%" . $input->getPost('sSearch_5') . "%' ");
        }

        if (($input->getPost('sSearch_6')) || ($input->getPost('sSearch_6') == '0')) {
            $where->literal("nik::text ILIKE '%" . str_ireplace("'", "''", $input->getPost('sSearch_6')) . "%'");
        }

        if (($input->getPost('sSearch_7')) || ($input->getPost('sSearch_7') == '0')) {
            $where->literal("npwp::text ILIKE '%" . $input->getPost('sSearch_7') . "%' ");
        }

        if (($input->getPost('sSearch_8')) || ($input->getPost('sSearch_8') == '0')) {
            $where->literal("nama_wp::text ILIKE '%" . $input->getPost('sSearch_8') . "%'");
        }

        if (($input->getPost('sSearch_9')) || ($input->getPost('sSearch_9') == '0')) {
            $where->literal("alamat_op::text ILIKE '%" . $input->getPost('sSearch_9') . "%' ");
        }
        if (($input->getPost('sSearch_10')) || ($input->getPost('sSearch_10') == '0')) {
            $where->literal("kelurahan_op::text ILIKE '%" . $input->getPost('sSearch_10') . "%' ");
        }

        if (($input->getPost('sSearch_11')) || ($input->getPost('sSearch_11') == '0')) {
            $where->literal("kecamatan_op::text ILIKE '%" . $input->getPost('sSearch_11') . "%'");
        }

        if (($input->getPost('sSearch_12')) || ($input->getPost('sSearch_12') == '0')) {
            $where->literal("kota_op::text ILIKE '%" . $input->getPost('sSearch_12') . "%'");
        }

        if (($input->getPost('sSearch_11')) || ($input->getPost('sSearch_11') == '0')) {
            $where->literal("luastanah_op::text ILIKE '%" . $input->getPost('sSearch_11') . "%'");
        }

        if (($input->getPost('sSearch_12')) || ($input->getPost('sSearch_12') == '0')) {
            $where->literal("luasbangunan_op::text ILIKE '%" . $input->getPost('sSearch_12') . "%'");
        }

        if (($input->getPost('sSearch_13')) || ($input->getPost('sSearch_13') == '0')) {
            $where->literal("ppat::text ILIKE '%" . $input->getPost('sSearch_13') . "%'");
        }

        if (($input->getPost('sSearch_14')) || ($input->getPost('sSearch_14') == '0')) {
            $where->literal("no_sertipikat::text ILIKE '%" . $input->getPost('sSearch_14') . "%'");
        }


        if (($input->getPost('sSearch_15')) || ($input->getPost('sSearch_15') == '0')) {
            $where->literal("no_akta::text ILIKE '%" . $input->getPost('sSearch_15') . "%'");
        }

        if ($input->getPost('sSearch')) {
            $where->literal("
                    aktaid::text ILIKE '%" . $input->getPost('sSearch') . "%'
                    OR nop::text ILIKE '%" . $input->getPost('sSearch') . "%'
                    OR nib::text ILIKE '%" . $input->getPost('sSearch') . "%'
                    OR nik::text ILIKE '%" . $input->getPost('sSearch') . "%'
                    OR npwp::text ILIKE '%" . $input->getPost('sSearch') . "%'
                    OR nama_wp ILIKE '%" . $input->getPost('sSearch') . "%'
                    OR nama_wp ILIKE '%" . $input->getPost('sSearch') . "%'    
                    ");
        }

        $select->where($where);

        $totaldata = $this->getjumlahdata_v2($select);
        $iTotal = $totaldata;

        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("tgl_akta DESC");
        }

        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(5);
                $select->offset(0);
                $no = 1;
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();


        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );

        foreach ($rResult as $aRow) {
            $row = array();
            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tgltransaksi'])) . " </center>",
                "<center>" . $aRow['no_akta'] . "</center>",
                // "<center>" . $aRow['aktaid'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['tgl_akta'])) . " </center>",
                "<center>" . $aRow['ppat'] . "</center>",
                "<center>" . $aRow['nop'] . "</center>",
                "<center>" . $aRow['ntpd'] . "</center>",
                $aRow['nib'],
                "<center>" . $aRow['koordinat_x'] . "</center>",
                "<center>" . $aRow['koordinat_y'] . "</center>",
                $aRow['nik'],
                "<center>" . $aRow['npwp'] . "</center>",
                "<center>" . $aRow['nama_wp'] . "</center>",
                // "<center>" . $aRow['alamat_op'] . "</center>",
                "<center>" . $aRow['kelurahan_op'] . "</center>",
                "<center>" . $aRow['kecamatan_op'] . "</center>",
                "<center>" . $aRow['kota_op'] . "</center>",
                "<center>" . $aRow['luastanah_op'] . "</center>",
                // "<center>" . $aRow['luasbangunan_op'] . "</center>",
                // "<center>" . $aRow['no_sertipikat'] . "</center>",
                "<center>" . $aRow['jenis_hak'] . "</center>",
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function getjumlahdata_v2($select)
    {
        $sql = new Sql($this->adapter);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function cekdatasertifikatbpn($data_get)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('t_datasertifikat_bpn');
        $where = new Where();
        $where->literal("t_tgltransaksi::date >= '" . date('Y-m-d', strtotime($data_get['tgl_akta1'])) . "' 
        and t_tgltransaksi::date <= '" . date('Y-m-d', strtotime($data_get['tgl_akta2'])) . "' ");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();

        return $res;
    }
}
