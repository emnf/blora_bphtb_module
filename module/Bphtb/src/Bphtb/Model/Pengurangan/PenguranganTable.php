<?php

namespace Bphtb\Model\Pengurangan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Where;

class PenguranganTable extends AbstractTableGateway {

    protected $table = 't_detailsptbphtb';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new \Bphtb\Model\Pendataan\SSPDBphtbBase());
        $this->initialize();
    }

    public function getjumlahdata($sTable, $count, $sWhere) {
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    public function semuadatapengurangan($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl) {
        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }

        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                            . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }
        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_pendaftaran') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        if ($s_tipe_pejabat == 2) {
            $wherelogin_atas = ' AND t_idnotarisspt = ' . $s_iduser . ' ';
            $wherelogin_default = ' WHERE t_idnotarisspt = ' . $s_iduser . ' ';
        } else {
            $wherelogin_atas = ' ';
            $wherelogin_default = ' ';
        }

        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " " . $wherelogin_atas . "";
        } else {
            $sWhere = " " . $wherelogin_default . " ";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . " FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit;

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata;

        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );

        foreach ($rResult as $aRow) {
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            if (!empty($aRow['p_idpemeriksaan'])) {
                $jmlpajak = "<span style='float:right;'>" . number_format($aRow['p_totalspt'], 0, ',', '.') . "</span>";
            } else {
                $jmlpajak = "<span style='float:right;'>" . number_format($aRow['t_totalspt'], 0, ',', '.') . "</span>";
            }

            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }

            if ($aRow['fr_validasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-check-square-o"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-minus-circle"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 2) {
                $warnatr = '<span style="background-color:blue;color: #fff;"> &nbsp; <i class="fa fa-fw fa-plus-circle"></i> </span> &nbsp;';
            } else {
                $warnatr = '';
            }

            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/tervalidasi.png">';
            } else {
                if (empty($aRow['t_verifikasispt'])) {
                    $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/belumdivalidasi.png">';
                } else {
                    $status_verifikasi = '<img title="Syarat Validasi Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/validasi_tidaklengkap.png">';
                }
            }

            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn_fr btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn_fr btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }

            $s_tipe_pejabat = $session['s_tipe_pejabat'];
            if (!empty($aRow['t_idpengurangan'])) {
                $pengurangan = '<a href="pengurangan/cetakskpengurangan?t_idspt=' . $aRow['t_idspt'] . '" target="__BLANK" class="btn btn-success btn-md"><i class="fa fa-fw fa-print"></i> S.K. Pengurangan</a>';
                if ($aRow['t_approve_pengurangan'] == TRUE) {
                    $status_approve = "<span style='color:blue'>Disetujui</span>";
                } else {
                    $status_approve = "<span style='color:red'>Tidak Disetujui</span>";
                }
            } else {
                $pengurangan = '<a href="pengurangan/inputpengurangan?t_idspt=' . $aRow['t_idspt'] . '" class="btn btn-primary btn-md">Pengurangan</a>';
                $status_approve = '';
            }

            $row = array("<center>" . $no . "</center>",
                "<center>" . $warnatr . " " . $t_kohirspt . "</center>",
                "<center>" . $novalidasi . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                $aRow['s_namanotaris'],
                "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</span>",
                $aRow['t_namawppembeli'],
                "<span style='float:right;'>" . number_format($aRow['jml_pajak_v1'], 0, ',', '.') . "</span>",
                "<center>" . $status_bayar . "</center>",
                "<center>" . $status_pendaftaran . "</center>",
                "<center>" . $status_verifikasi . "</center>",
                "<center>" . $pengurangan . "<br>" . $status_approve . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }
        return $output;
    }

    public function update_t_spt($post) {
        if($post['t_approve_pengurangan'] == 2){ //TIDAK DISETUJUI
            $data = array(
                't_approve_pengurangpembebas' => FALSE
            );
            
        }else{ //SETUJU
            $data = array(
                // Data Pajak
                't_totalspt' => str_ireplace(".", "", $post->t_totalpajak),
                't_approve_pengurangpembebas' => TRUE
            );
            
        }
        
        $t_spt = new \Zend\Db\TableGateway\TableGateway('t_spt', $this->adapter);
        $t_spt->update($data, array('t_idspt' => $post->t_idspt));
    }

    public function update_pembayaran($post) {
        if($post['t_approve_pengurangan'] == 2){ //TIDAK DISETUJUI
            $data = array(
                't_approve_pengurangpembebas' => FALSE
            );
            
        }else{ //SETUJU

            $data = array(
                // Data Pajak
                't_nilaipembayaranspt' => str_ireplace(".", "", $post->t_totalpajak)
            );
            //var_dump($data);exit();
        }   
        
        $t_pembayaranspt = new \Zend\Db\TableGateway\TableGateway('t_pembayaranspt', $this->adapter);
        $t_pembayaranspt->update($data, array('t_idspt' => $post->t_idspt));
    }

    

    public function simpan_t_pengurangan($post, $session) {
        if($post['t_approve_pengurangan'] == 2){ //TIDAK DISETUJUI
            $approve = FALSE;
        }else{ //SETUJU
            $approve = TRUE;
        }
        
        if(!empty($post->t_pengurangan) && $post->t_pengurangan != ""){
            $pengurangan = str_ireplace(".", "", $post->t_pengurangan);
        }else{
            $pengurangan = 0;
        }
        
        if(!empty($post->t_totalpajak) &&  $post->t_totalpajak != ""){
            $totalpajak = str_ireplace(".", "", $post->t_totalpajak);
        }else{
            $totalpajak = str_ireplace(".", "", $post->t_totalspt);
        }
        
        $data = array(
            't_idsptpengurangan' => $post->t_idspt,
            't_totalsptsebelumnya' => str_ireplace(".", "", $post->t_totalspt),
            't_persenpengurangan' => $post->t_persenpengurangan,
            't_pengurangan' => $pengurangan,
            't_totalpajak' => $totalpajak,
            't_tglpengurangan' => date('Y-m-d'),
            't_penginputpengurangan' => $session['s_iduser'],
            't_nosk' => $post->t_nosk,
            't_keterangan' => $post->t_keterangan,
            't_approve_pengurangan' => $approve
        );
        $t_pengurangan = new \Zend\Db\TableGateway\TableGateway('t_pengurangan', $this->adapter);
        $t_pengurangan->insert($data);
    }

    public function getDataHasilPengurangan($tgl_awal, $tgl_akhir) {
        $sql = "SELECT * FROM t_pengurangan a
                LEFT JOIN  view_sspd b on b.t_idspt=a.t_idsptpengurangan
                LEFT JOIN s_users c on c.s_iduser = a.t_penginputpengurangan
                where a.t_tglpengurangan BETWEEN '" . date('Y-m-d', strtotime($tgl_awal)) . "' and '" . date('Y-m-d', strtotime($tgl_akhir)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

}
