<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class TarifBphtbTable extends AbstractTableGateway {

    protected $table = 's_tarifbphtb';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new TarifBphtbBase());
        $this->initialize();
    }
    
    public function ambilsatudata($query) {
        $sql = $query;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }
    
    public function simpandata($query) {
        $sql = $query;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }
    
    //================= datagrid pembayaran
    public function getjumlahdata($sTable, $count, $sWhere) {
        $sql = "SELECT ".$count." FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }
    
    public function semuadatatarifbphtb($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl) {
        
        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }

      
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                            . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }
    
        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY ".$order_default."";
        }

        $iColumnCount = count($aColumns);
        
        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }else{
                            $tanggalcari = "" .$tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    }else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }

       
        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));
                
                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }else{
                        $tanggalcari = "" .$tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                }else {
                    if($aColumns[$i] == 's_idjenistransaksi'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }elseif($aColumns[$i] == 't_idnotarisspt'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }elseif($aColumns[$i] == 't_statusbayarspt'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }elseif($aColumns[$i] == 'status_validasi'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }else{
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        

        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules)." ";
        } else {
            $sWhere = " ";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
        
        //var_dump($sql);
        //exit();
        
        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();
        
        
        
        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];
        
        
        
        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal, 
            "aaData" => array(),
        );
        
        

        foreach ($rResult as $aRow) {
            $row = array();
            
            $btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['s_idtarifbphtb'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> '; //<a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['s_idtarifbphtb'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            //s_idtarifbphtb', 's_tarifbphtb', 's_dasarhukumtarifbphtb','s_tanggaltarifbphtb','s_statustarifbphtb
            if($aRow['s_statustarifbphtb'] == 2){
                $status = 'Aktif';
            }else{
                $status = 'Tidak Aktif';
            }
            $row = array($no, $aRow['s_tarifbphtb'], $aRow['s_dasarhukumtarifbphtb'], substr($aRow['s_tanggaltarifbphtb'], 0,4),$status , $btn);
            $output['aaData'][] = $row;
            $no++;
        }
        
        return $output;
    }
    //=========================== datagrid tarif bphtb

    public function savedata(TarifBphtbBase $kc) {
        $data = array(
            's_tarifbphtb' => $kc->s_tarifbphtb,
            's_dasarhukumtarifbphtb' => $kc->s_dasarhukumtarifbphtb,
            's_tanggaltarifbphtb' => date('Y-m-d', strtotime($kc->s_tanggaltarifbphtb)),
            's_statustarifbphtb' => $kc->s_statustarifbphtb
        );
        $id = (int) $kc->s_idtarifbphtb;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idtarifbphtb' => $kc->s_idtarifbphtb));
        }
    }

    public function getGridCount(TarifBphtbBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if($base->s_tarifbphtb != 'undefined')
            $where->literal("$this->table.s_tarifbphtb::text LIKE '%$base->s_tarifbphtb%'");     
        if($base->s_tanggaltarifbphtb != 'undefined')
            $where->literal("$this->table.s_tanggaltarifbphtb::text LIKE '%$base->s_tanggaltarifbphtb%'");     
        if($base->s_dasarhukumtarifbphtb != 'undefined')
            $where->literal("$this->table.s_dasarhukumtarifbphtb::text LIKE '%$base->s_dasarhukumtarifbphtb%'");
        if($base->s_statustarifbphtb != 'undefined')
            $where->literal("$this->table.s_statustarifbphtb::text LIKE '%$base->s_statustarifbphtb%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(TarifBphtbBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if($base->s_tarifbphtb != 'undefined')
            $where->literal("$this->table.s_tarifbphtb::text LIKE '%$base->s_tarifbphtb%'");     
        if($base->s_tanggaltarifbphtb != 'undefined')
            $where->literal("$this->table.s_tanggaltarifbphtb::text LIKE '%$base->s_tanggaltarifbphtb%'");     
        if($base->s_dasarhukumtarifbphtb != 'undefined')
            $where->literal("$this->table.s_dasarhukumtarifbphtb::text LIKE '%$base->s_dasarhukumtarifbphtb%'");
        if($base->s_statustarifbphtb != 'undefined')
            $where->literal("$this->table.s_statustarifbphtb::text LIKE '%$base->s_statustarifbphtb%'");
        $select->where($where);
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idtarifbphtb' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id) {
        $this->delete(array('s_idtarifbphtb' => $id));
    }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }

}
