<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

class ButtonTable extends AbstractTableGateway
{

    protected $table = "s_button";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new ButtonBase());
        $this->initialize();
    }

    public function getData()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_button");
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function simpan($post)
    {
        $sql = new Sql($this->adapter);
        $data = [
            "s_status_button" => $post["s_status_button"]
        ];

        $query = $sql->update("s_button")->set($data)->where(["s_id_button" => $post["s_id_button"]]);
        $res = $sql->prepareStatementForSqlObject($query)->execute();
        return $res;
    }

    public function getDataButtonId($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_button")->where(["s_id_button" => (int) $id]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }
}
