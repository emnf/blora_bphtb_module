<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class HakTanahTable extends AbstractTableGateway {

    protected $table = 's_jenishaktanah';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new HakTanahBase());
        $this->initialize();
    }

    public function savedata(HakTanahBase $kc) {
        $data = array(
            's_kodehaktanah' => $kc->s_kodehaktanah,
            's_namahaktanah' => $kc->s_namahaktanah,
        );
        $id = (int) $kc->s_idhaktanah;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idhaktanah' => $kc->s_idhaktanah));
        }
    }

    public function getGridCount(HakTanahBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if($base->s_kodehaktanah != 'undefined')
            $where->literal("$this->table.s_kodehaktanah::text LIKE '%$base->s_kodehaktanah%'");     
        if($base->s_namahaktanah != 'undefined')
            $where->literal("$this->table.s_namahaktanah::text LIKE '%$base->s_namahaktanah%'");  
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(HakTanahBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new \Zend\Db\Sql\Where();
        if($base->s_kodehaktanah != 'undefined')
            $where->literal("$this->table.s_kodehaktanah::text LIKE '%$base->s_kodehaktanah%'");     
        if($base->s_namahaktanah != 'undefined')
            $where->literal("$this->table.s_namahaktanah::text LIKE '%$base->s_namahaktanah%'");  
        $select->where($where);
        $select->order("s_kodehaktanah ASC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idhaktanah' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id) {
        $this->delete(array('s_idhaktanah' => $id));
    }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }

}
