<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class PersyaratanTable extends AbstractTableGateway
{

    protected $table = 'view_s_persyaratan';
    protected $table_transaksi = "s_jenistransaksi";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PersyaratanBase());
        $this->initialize();
    }

    public function savedata(PersyaratanBase $pb)
    {
        $data = array(
            's_idjenistransaksi' => $pb->s_idjenistransaksi,
            's_namapersyaratan' => $pb->s_namapersyaratan,
        );
        $id = (int) $pb->s_idpersyaratan;
        if ($id == 0) {
            $table_persyaratan = new \Zend\Db\TableGateway\TableGateway('s_persyaratan', $this->adapter);
            $table_persyaratan->insert($data);
        } else {
            $table_persyaratan = new \Zend\Db\TableGateway\TableGateway('s_persyaratan', $this->adapter);
            $table_persyaratan->update($data, array('s_idpersyaratan' => $pb->s_idpersyaratan));
        }
    }

    public function getGridCount(PersyaratanBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        $select->join($this->table_transaksi, "$this->table_transaksi.s_idjenistransaksi = $this->table.s_idjenistransaksi", array(), "left");
        if ($base->s_namapersyaratan != 'undefined')
            $where->literal("$this->table.s_namapersyaratan::text LIKE '%$base->s_namapersyaratan%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("$this->table_transaksi.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(PersyaratanBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        $select->join($this->table_transaksi, "$this->table_transaksi.s_idjenistransaksi = $this->table.s_idjenistransaksi", array('s_namajenistransaksi'), 'left');
        if ($base->s_namapersyaratan != 'undefined')
            $where->literal("$this->table.s_namapersyaratan::text LIKE '%$base->s_namapersyaratan%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("$this->table_transaksi.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        $select->where($where);
        $select->order("s_idjenistransaksi asc");
        $select->order("s_idpersyaratan asc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id)
    {
        $rowset = $this->select(array('s_idpersyaratan' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getpersyaratan($id)
    {

        $sql = "select * from s_persyaratan where s_idjenistransaksi=" . $id . "";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getDataIdTransaksis($id)
    {
        $rowset = $this->select(array('s_idjenistransaksi' => $id));
        return $rowset;
    }

    public function getDataSyaratSPT($idsyarat, $idspt)
    {
        $sql = "select t_persyaratan from t_spt where t_idspt=$idspt and t_persyaratan  like '%$idsyarat%'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function hapusData($id)
    {
        $table_persyaratan = new \Zend\Db\TableGateway\TableGateway('s_persyaratan', $this->adapter);
        $table_persyaratan->delete(array('s_idpersyaratan' => $id));
    }

    public function comboBox($s_idjenistransaksi)
    {
        $rowset = $this->select(array('s_idjenistransaksi' => $s_idjenistransaksi));
        return $rowset;
    }

    public function syaratfileupload($s_idjenistransaksi, $idspt)
    {
        // $sql = "select a.*,b.letak_file,b.nama_file from s_persyaratan a
        //       left join t_filesyarat b ON a.s_idjenistransaksi = b.s_idjenistransaksi and a.s_idpersyaratan = b.s_idpersyaratan and t_idspt = " . $idspt . " where a.s_idjenistransaksi = " . $s_idjenistransaksi . "";
        // $statement = $this->adapter->query($sql);
        // $res = $statement->execute();
        // return $res;

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "s_persyaratan"]);
        $select->join(["b" => "t_filesyarat"], new Expression("
        a.s_idjenistransaksi = b.s_idjenistransaksi 
        and a.s_idpersyaratan = b.s_idpersyaratan and t_idspt = " . $idspt . ""), [
            "letak_file", "nama_file", "id_filesyarat"
        ], "LEFT");
        $select->where(["a.s_idjenistransaksi" => $s_idjenistransaksi]);
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function syaratfileupload_danfilenya($s_idjenistransaksi, $idspt)
    {
        $sql = "select a.*,b.letak_file,b.nama_file 
                from s_persyaratan a
                    left join t_filesyarat b ON a.s_idjenistransaksi = b.s_idjenistransaksi and a.s_idpersyaratan = b.s_idpersyaratan and b.t_idspt = " . $idspt . "
                    where a.s_idjenistransaksi = " . $s_idjenistransaksi . "";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getDataPesyaratanUploadByIdFileSyarat($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("t_filesyarat");
        $select->where(["id_filesyarat" => $id]);
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res->current();
    }
}
