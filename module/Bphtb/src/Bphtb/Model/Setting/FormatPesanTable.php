<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class FormatPesanTable extends AbstractTableGateway
{

    protected $table = "s_format_pesan";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new FormatPesanBase());
        $this->initialize();
    }

    public function getGridCount($select)
    {
        $sql = new Sql($this->adapter);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->count();
        return $res;
    }

    public function getGridData(FormatPesanBase $base, $post)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        $select->where($where);

        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getGridCount($select);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        $start = $limit * $page - $limit;
        if ($start < 0) {
            $start = 0;
        }

        $select->limit((int) $limit);
        $select->offset((int) $start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return [
            "res" => $res,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_pages" => $total_pages,
        ];
    }

    public function simpan($post)
    {

        $ketJnsPesan = [
            "1" => "BERKAS"
        ];

        $sql = new Sql($this->adapter);
        $data = [
            "s_id_jns_pesan" => $post["s_id_jns_pesan"],
            "s_ket_jns_pesan" => $ketJnsPesan[$post["s_id_jns_pesan"]],
            "s_isian_pesan" => $post["s_isian_pesan"]
        ];

        $id = (int) $post["s_id_format_pesan"];
        if ($id == 0) {
            $query = $sql->insert("s_format_pesan")->values($data);
        } else {
            $query = $sql->update("s_format_pesan")->set($data)->where(["s_id_format_pesan" => $id]);
        }

        $res = $sql->prepareStatementForSqlObject($query)->execute();
        return $res;
    }

    public function hapus($id)
    {
        return $this->delete(["s_id_format_pesan" => $id]);
    }

    public function comboBox()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_format_pesan");
        $res = $sql->prepareStatementForSqlObject($select)->execute();

        $ar =  [];
        foreach ($res as $res) {
            $ar[$res["s_id_format_pesan"]] = $res["s_isian_pesan"];
        }
        return $ar;
    }

    public function getFormatPesanId($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_format_pesan")->where(["s_id_format_pesan" => $id]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }
}
