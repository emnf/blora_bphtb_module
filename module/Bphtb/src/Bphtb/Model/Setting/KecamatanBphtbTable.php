<?php
namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;

class KecamatanBphtbTable extends AbstractTableGateway {
    
    protected $table = 's_kecamatan';
    
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new KecamatanBphtbBase());
        $this->initialize();
    }

    public function checkExist(KecamatanBphtbBase $kc){
        $rowset = $this->select(array('s_kodekecamatan' => $kc->s_kodekecamatan));
        $row = $rowset->current();
        return $row;
    }

    public function checkId(KecamatanBphtbBase $kc){
        $rowset = $this->select(array('s_idkecamatan' => $kc->s_idkecamatan));
        $row = $rowset->current();
        return $row;
    }
    
    public function savedata(KecamatanBphtbBase $kc) {
        $data = array(            
            's_kodekecamatan'=>$kc->s_kodekecamatan,
            's_namakecamatan'=>$kc->s_namakecamatan            
        );
        $id = (int) $kc->s_idkecamatan;
        if ($id == 0) {
            $this->insert($data);
        } else {
            if ($this->checkId($kc)) {
                $this->update($data, array('s_idkecamatan' => $kc->s_idkecamatan));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function save($kc){
        $data = array(
            's_kodekecamatan' => $kc['s_kodekecamatan'],
            's_namakecamatan' => $kc['s_namakecamatan']
        );
        
        $this->insert($data);
        
    }
    
    public function checkEmpty(){
        $resultSet = $this->select();
        return $resultSet->count();
    }

    public function getGridCount(KecamatanBphtbBase $kb){        
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);        
        //$select->order($kb->sidx . " " . $kb->sord);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }
    
    public function getGridData(KecamatanBphtbBase $kb, $offset){        
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $select->order($kb->sidx . " " . $kb->sord);        
        $select->limit($kb->rows = (int) $kb->rows);
        $select->offset($offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getDataId($id){
        $rowset = $this->select(array('s_idkecamatan' => $id));
        $row = $rowset->current();
        return $row;
    }
    
    public function hapusData(KecamatanBphtbBase $kb){
        $this->delete(array('s_idkecamatan' => $kb->s_idkecamatan));
    }
    
    public function comboBox(){
        $resultSet = $this->select();
        return $resultSet;
    }

    public function getIdKecamatan($kd){
        $resultSet = $this->select(array('s_kodekecamatan'=>$kd));
        return $resultSet->current();   
    }
    
}