<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class KelurahanBphtbTable extends AbstractTableGateway {

    protected $table = 's_kelurahan';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new KelurahanBphtbBase());
        $this->initialize();
    }

    public function checkExist(KelurahanBphtbBase $kb) {
        $rowset = $this->select(array('s_kodekelurahan' => $kb->s_kodekelurahan));
        $row = $rowset->current();
        return $row;
    }

    public function checkId(KelurahanBphtbBase $kb) {
        $rowset = $this->select(array('s_idkelurahan' => $kb->s_idkelurahan));
        $row = $rowset->current();
        return $row;
    }

    public function savedata(KelurahanBphtbBase $kb) {
        $data = array(
            's_idkecamatan' => $kb->s_idkecamatan,
            's_kodekelurahan' => $kb->s_kodekelurahan,
            's_namakelurahan' => $kb->s_namakelurahan
        );
        $id = (int) $kb->s_idkelurahan;
        if ($id == 0) {
            $this->insert($data);
        } else {
            if ($this->checkId($kb)) {
                $this->update($data, array('s_idkelurahan' => $kb->s_idkelurahan));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function save($kc){
        $data = array(
            's_idkecamatan' => $kc['s_idkecamatan'],
            's_kodekelurahan' => $kc['s_kodekelurahan'],
            's_namakelurahan' => $kc['s_namakelurahan']
        );
        
        $this->insert($data);
        
    }
    
    public function checkEmpty(){
        $resultSet = $this->select();
        return $resultSet->count();
    }

    public function getGridCount(KelurahanBphtbBase $kb) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table)
                ->join('s_kecamatan', 's_kecamatan.s_idkecamatan = s_kelurahan.s_idkecamatan');
                
        $select->order($kb->sidx . " " . $kb->sord);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(KelurahanBphtbBase $kb, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table)
                ->join('s_kecamatan', 's_kecamatan.s_idkecamatan = s_kelurahan.s_idkecamatan');
        $select->order($kb->sidx . " " . $kb->sord);
        $select->limit($kb->rows = (int) $kb->rows);
        $select->offset($offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($kb) {
        $rowset = $this->select(array('s_idkelurahan' => $kb));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData(KelurahanBphtbBase $kb) {
        $this->delete(array('s_idkelurahan' => $kb->s_idkelurahan));
    }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }
    
    public function getByKecamatan(KelurahanBphtbBase $kb){        
        $resultSet = $this->select(array('s_idkecamatan' => $kb->s_idkecamatan));
        return $resultSet;
    }
    
    public function getByKecamatan2($id){
        $resultSet = $this->select(array('s_idkecamatan' => $id));
        return $resultSet;
    }

}
