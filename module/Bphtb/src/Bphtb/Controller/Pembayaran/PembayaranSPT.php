<?php

// Modul Pembayaran

namespace Bphtb\Controller\Pembayaran;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Bphtb\Form\Pendataan\SSPDFrm;
use Bphtb\Model\Pendataan\SSPDBphtbBase;

class PembayaranSPT extends AbstractActionController
{

    protected $tbl_pembayaran, $tbl_pendataan, $tbl_jenistransaksi, $tbl_haktanah, $tbl_pemda, $tbl_sspd, $tbl_pejabat;

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'

    }

    // Index Pembayaran
    public function indexAction()
    {
        // $session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $ar_pemda = $this->getPemda()->getdata();

        $data_mengetahui = $this->getTblPejabat()->getdata();
        $data_mengetahui_periksa = $this->getTblPejabat()->getdata();
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $form = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $view = new ViewModel(array(
            'form' => $form,
            'data_mengetahui' => $data_mengetahui,
            'data_mengetahui_periksa' => $data_mengetahui_periksa,
            'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_pembayaran' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_pembayaran_v5';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $aColumns = array('t_idspt', 't_kohirspt', 't_kohirketetapanspt', 's_idjenistransaksi', 't_tglverifikasispt', 't_namawppembeli', 't_nilaipembayaranspt', 't_kodebayarbanksppt', 't_statusbayarspt', 'status_validasi', 's_namajenistransaksi', 't_persyaratan', 't_verifikasispt', 't_idjenistransaksi', 't_inputbpn', 't_periodespt', 'status_pendaftaran', 't_idpembayaranspt', 'p_totalspt', 'p_idpemeriksaan', 't_totalspt', 't_ketetapanspt');


        $panggildata = $this->getServiceLocator()->get("PembayaranSptTable");
        $rResult = $panggildata->semuadatapembayaran($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    // Daftar Pada Index Pembayaran (SSPD/SKPDKB)
    public function dataGrid2Action()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getTblPembayaran()->getGridCount($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->getTblPembayaran()->getGridData($base, $start);
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            if ($row['t_ketetapanspt'] == 1) {
                if ($row['t_inputbpn'] == true) {
                    $s .= "<td> <span class='badge' style='background-color:#CC0000;'>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . " </span></td>";
                } else {
                    $s .= "<td>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</td>";
                }
                $s .= "<td> SSPD </td>";
            } else {
                if ($row['t_ketetapands'] == 3) {
                    $s .= "<td> <span class='badge' style='background-color:green;'>" . $row['t_kohirds'] . " </span></td>";
                    $s .= "<td> DENDA </td>";
                    $notarisds = $row['notarisds'];
                } elseif ($row['t_ketetapands'] == 4) {
                    $s .= "<td> <span class='badge' style='background-color:green;'>" . $row['t_kohirds'] . " </span></td>";
                    $s .= "<td> SANKSI </td>";
                    $notarisds = $row['notarisds'];
                }
            }

            $s .= "<td>" . $row['t_periodepembayaran'] . "</td>";
            $s .= "<td>" . date('d-m-Y', strtotime($row['t_tanggalpembayaran'])) . "</td>";
            if ($row['t_ketetapanspt'] == 1) {
                $s .= "<td>" . $row['t_namawppembeli'] . "</td>";
            } else {
                $s .= "<td>" . $notarisds . "</td>";
            }
            $result_array_syarat = \Zend\Json\Json::decode($row['t_persyaratan']);
            $jml_syarat = count($result_array_syarat);
            $result_array_syarat_verifikasi = \Zend\Json\Json::decode($row['t_verifikasispt']);
            $jml_syarat_verifikasi = count($result_array_syarat_verifikasi);
            if ($row['t_ketetapanspt'] == 1) {
                $cektabelpersyaratan = $this->getTblJenTran()->jumlahsyarat($row['t_idjenistransaksi']);
                if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
                    $status_verifikasi = "Tervalidasi";
                } else {
                    $status_verifikasi = "Belum Tervalidasi";
                }
            } else {
                $status_verifikasi = '-';
            }
            $s .= "<td>" . $status_verifikasi . "</td>";
            $s .= "<td>" . $row['t_kodebayarbanksppt'] . "</td>";
            if ($row['t_statusbayarspt'] == true) {
                $status_bayar = "Sudah Dibayar";
            } else {
                $status_bayar = "Belum Dibayar";
            }
            $s .= "<td>" . $status_bayar . "</td>";
            $s .= "<td>" . number_format($row['t_nilaipembayaranspt'], 0, ',', '.') . "</td>";
            if ($row['t_ketetapanspt'] == 1) {
                $s .= "<td><a href='pembayaran_sptbphtb/cetakbuktivalidasipembayaran?&action=cetakbuktivalidasipembayaran&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat' style='width:130px'>Validasi Pembayaran</a> <a href='pembayaran_sptbphtb/cetakbuktipembayaran?&action=cetakbuktipembayaran&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat' style='width:130px'>Bukti Pembayaran</a></td>";
                $s .= "<td><a href='pembayaran_sptbphtb/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:70px'>Lihat</a> <a href='#' onclick='hapus(" . $row['t_idpembayaranspt'] . ");return false;' class='btn btn-danger btn-sm btn-flat' style='width:70px'>Hapus</a></td>";
            } else {
                $s .= "<td>-</td>";
                $s .= "<td><a href='#' onclick='hapus(" . $row['t_idpembayaranspt'] . ");updatepemeriksaan(" . $row['p_idpemeriksaan'] . ");return false;' class='btn btn-danger btn-sm btn-flat' style='width:70px'>Hapus</a></td>";
            }
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function viewdataAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getPendataanBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_pembayaran' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function viewdata2Action()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getPendataanBphtb()->getDataId($id);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_pembayaran' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // Daftar SSPD
    // Lokasi : Button Cari SSPD Tambah Pembayaran
    public function dataGridPendataanBphtbAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_pembayaran_v5';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $aColumns = array('t_idspt', 't_kohirspt', 's_idjenistransaksi', 't_tglprosesspt', 't_kodebayarbanksppt', 't_namawppembeli', 't_namawppenjual', 't_nopbphtbsppt', 'jml_pajak_v1',  't_totalspt', 't_statusbayarspt', 'status_validasi', 's_namajenistransaksi', 't_persyaratan', 't_verifikasispt', 't_idjenistransaksi', 't_inputbpn', 't_periodespt', 'status_pendaftaran', 't_idpembayaranspt', 'p_totalspt', 'p_idpemeriksaan');


        $panggildata = $this->getServiceLocator()->get("PembayaranSptTable");
        $rResult = $panggildata->semuadatatervalidasi($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGridPendataanBphtb2Action()
    {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_idspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;
        $count = $this->getPendataanBphtb()->getGridCountPembayaran($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array(
            'Content-type' => 'text/xml'
        ));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";
        $data = $this->getPendataanBphtb()->getGridDataPembayaran($sortname, $sortorder, $query, $qtype, $start, $rp);
        foreach ($data as $row) {
            $s .= "<row id='" . $row['t_idspt'] . "'>";
            $s .= "<cell>" . $row['t_kodebayarbanksppt'] . "</cell>";
            $s .= "<cell>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</cell>";
            $s .= "<cell>" . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . "</cell>";
            $s .= "<cell>" . $row['t_nopbphtbsppt'] . "</cell>";
            $s .= "<cell>" . $row['t_namawppembeli'] . "</cell>";
            $s .= "<cell>" . $row['t_namawppenjual'] . "</cell>";
            $s .= "<cell><![CDATA[<a href='#' class='btn btn-xs btn-warning' onclick='pilihPendataanSspdBphtb(" . $row['t_idspt'] . ");return false;' >PILIH</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    // Pilih Salah Satu Daftar Pembayaran SSPD
    // Lokasi : Button Cari SSPD Tambah Pembayaran
    public function pilihPendataanSspdBphtbAction()
    {
        $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getPendataanBphtb()->getPendataanSspdBphtb($ex);
                if (!empty($data['p_idpemeriksaan'])) {
                    $data['t_luastanah'] = str_ireplace('.', '', $data['p_luastanah']) / 100;
                    $data['t_luasbangunan'] = str_ireplace('.', '', $data['p_luasbangunan']) / 100;
                    $data['t_njoptanah'] = $data['p_njoptanah'];
                    $data['t_njopbangunan'] = $data['p_njopbangunan'];
                    $data['t_totalnjoptanah'] = $data['p_totalnjoptanah'];
                    $data['t_totalnjopbangunan'] = $data['p_totalnjopbangunan'];
                    $data['t_grandtotalnjop'] = $data['p_grandtotalnjop'];
                    $data['t_nilaitransaksispt'] = $data['p_nilaitransaksispt'];
                    $data['t_potonganspt'] = $data['p_potonganspt'];
                    $data['t_totalspt'] = $data['p_totalspt'];
                    $data['t_grandtotalnjop_aphb'] = $data['p_grandtotalnjop_aphb'];
                } else {
                    $data['t_luastanah'] = str_ireplace('.', '', $data['t_luastanah']) / 100;
                    $data['t_luasbangunan'] = str_ireplace('.', '', $data['t_luasbangunan']) / 100;
                }

                $aphb_kali = $data['t_tarif_pembagian_aphb_kali'];
                $aphb_bagi = $data['t_tarif_pembagian_aphb_bagi'];
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($data['t_grandtotalnjop'] >= $data['t_nilaitransaksispt']) {
                        $data['npop'] = $data['t_grandtotalnjop'];
                    } else {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    }
                } else {
                    if ($data['t_grandtotalnjop_aphb'] >= $data['t_nilaitransaksispt']) {
                        $data['npop'] = $data['t_grandtotalnjop_aphb'];
                    } else {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    }
                }

                $data['t_tarif_pembagian_aphb_kali'] = $data['t_tarif_pembagian_aphb_kali'];
                $data['t_tarif_pembagian_aphb_bagi'] = $data['t_tarif_pembagian_aphb_bagi'];
                $data['t_grandtotalnjop_aphb'] = number_format($data['t_grandtotalnjop_aphb'], 0, ',', '.');
                $data['t_potongan_waris_hibahwasiat'] = $data['t_potongan_waris_hibahwasiat'];
                $data['t_idjenistransaksi'] = $data['t_idjenistransaksi'];


                $data['t_npopkpspt'] = $data['npop'] - $data['t_potonganspt'];
                $data['t_persenbphtb'] = $data['t_persenbphtb'];






                if (!empty($data['t_idsptsebelumnya'])) {
                    $idsebelumnya = $this->getTblPembayaran()->getDataPembayaran($data['t_idsptsebelumnya']);
                    $data['pembayaransebelumnya'] = $idsebelumnya['t_nilaipembayaranspt'];
                } else {
                    $data['pembayaransebelumnya'] = 0;
                }
                $data['t_nilaipembayaranspt'] = $data['t_totalspt'] - $data['pembayaransebelumnya'];
                $data['t_luastanah'] = number_format($data['t_luastanah'], 0, ',', '.');
                $data['t_luasbangunan'] = number_format($data['t_luasbangunan'], 0, ',', '.');
                $data['t_totalnjoptanah'] = number_format($data['t_totalnjoptanah'], 0, ',', '.');
                $data['t_totalnjopbangunan'] = number_format($data['t_totalnjopbangunan'], 0, ',', '.');
                $data['t_njoptanah'] = number_format($data['t_njoptanah'], 0, ',', '.');
                $data['t_njopbangunan'] = number_format($data['t_njopbangunan'], 0, ',', '.');
                $data['t_grandtotalnjop'] = number_format($data['t_grandtotalnjop'], 0, ',', '.');
                $data['t_nilaitransaksispt'] = number_format($data['t_nilaitransaksispt'], 0, ',', '.');
                $data['npop'] = number_format($data['npop'], 0, ',', '.');
                $data['t_npopkpspt'] = number_format($data['t_npopkpspt'], 0, ',', '.');
                $data['t_potonganspt'] = number_format($data['t_potonganspt'], 0, ',', '.');
                $data['t_totalspt'] = number_format($data['t_totalspt'], 0, ',', '.');
                $data['t_nilaipembayaranspt'] = number_format($data['t_nilaipembayaranspt'], 0, ',', '.');
                $data['pembayaransebelumnya'] = number_format($data['pembayaransebelumnya'], 0, ',', '.');
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function dataGridDSAction()
    {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_idspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;
        $count = $this->getPendataanBphtb()->getGridCountDS($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array(
            'Content-type' => 'text/xml'
        ));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";
        $data = $this->getPendataanBphtb()->getGridDataDS($sortname, $sortorder, $query, $qtype, $start, $rp);
        foreach ($data as $row) {
            $s .= "<row id='" . $row['t_idds'] . "'>";
            $s .= "<cell>" . $row['t_kodebayards'] . "</cell>";
            $s .= "<cell>" . str_pad($row['t_kohirds'], 4, '0', STR_PAD_LEFT) . "</cell>";
            $s .= "<cell>" . date('d-m-Y', strtotime($row['t_tglprosesds'])) . "</cell>";
            $s .= "<cell>" . date('d-m-Y', strtotime($row['t_periodeds'])) . "</cell>";
            $s .= "<cell>" . $row['s_namanotaris'] . "</cell>";
            $s .= "<cell>" . $row['s_alamatnotaris'] . "</cell>";
            $s .= "<cell>" . number_format($row['t_jumlahds'], 0, ',', '.') . "</cell>";
            $s .= "<cell><![CDATA[<a href='#' onclick='pilihPendataanDS(" . $row['t_idds'] . ");return false;' >Pilih</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    public function pilihPendataanDSAction()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getPendataanBphtb()->getPendataanDS($ex->t_idds);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Simpan Pembayaran SSPD / SKPDKB
    // Lokasi : Tambah Pembayaran
    public function tambahAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $ar_pemda = $this->getPemda()->getdata();
        $req = $this->getRequest();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                if ($kb->jenis_pembayaran == 1) {
                    //============= Model\Pembayaran\PembayaranSptTable
                    $data = $this->getTblPembayaran()->getDataPembayaran($kb->t_idspt);
                    if (!empty($data['t_idspt'])) {
                        //============= Model\Pembayaran\PembayaranSptTable    
                        $this->getTblPembayaran()->updatedatapembayaranTrue($kb, $session['s_iduser']);
                    } else {
                        //============= Model\Pembayaran\PembayaranSptTable    
                        $this->getTblPembayaran()->savedataPembayaran($kb, $session['s_iduser']);
                    }
                } elseif ($kb->jenis_pembayaran == 2 || $kb->jenis_pembayaran == 3) {
                    $this->getTblPembayaran()->savedataPembayaranDenda($kb, $session['s_iduser']);
                }
                return $this->redirect()->toRoute('pembayaran_sptbphtb');
            }
        }

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $view = new ViewModel(array(
            'frm' => $frm,
            'datajenistransaksi' => $datajenistransaksi
        ));

        $data = array(
            'menu_pembayaran' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // Hapus Pembayaran SSPD / SKPDKB
    // Lokasi : Index Pembayaran
    public function HapusAction()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                $data = $this->getTblPembayaran()->hapusData($kb);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Hapus Pembayaran SSPD / SKPDKB
    // Lokasi : Index Pembayaran
    public function updatepemeriksaanAction()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                $data = $this->getTblPembayaran()->updatedatapemeriksaanFalse($kb);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Onchange No SSPD
    // Lokasi Tambah Pembayaran
    public function datasspdAction()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $this->getTblPembayaran()->updatedatapemeriksaanFalse($ex);
                $data = $this->getTblPembayaran()->temukanDataSspd($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Onchange No SKPDKKB
    // Lokasi Tambah Pembayaran
    public function dataskpdkbAction()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getTblPembayaran()->temukanDataSkpdkb($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Kontrol Batas Waktu Pembayaran SSPD (Maksimal 3 Hari) dari tabel s_tempo
    // Lokasi : Tambah Pembayaran
    public function validitassspdAction()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $datahari = $this->getTblPembayaran()->cekJumlahhari();
                $data = $this->getTblPembayaran()->cekValiditasSSPD($ex, $datahari['s_jumlahhari']);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetakpencatatansetoranbphtbAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                //============================ Model\Pencetakan\SSPDTable
                $ar_PencatatanSetoran = $this->getSSPD()->getDataPencatatanSetoranBulanan($data_get->tgl_setor1, $data_get->tgl_setor2); //getDataPencatatanSetoran
                $ar_tglcetak = $data_get->tgl_cetakan;
                //$ar_periodespt = $base->periode_spt;
                $ar_pemda = $this->getPemda()->getdata();
            }
            $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
            $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->mengetahui_periksa);
        }
        $view = new ViewModel(array(
            'data_pencatatanSetoran' => $ar_PencatatanSetoran,
            'tgl_cetakan' => $ar_tglcetak,
            'tgl_setor1' => $data_get->tgl_setor1,
            'tgl_setor2' => $data_get->tgl_setor2,
            'ar_pemda' => $ar_pemda,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakpencatatansetoranbphtb2Action()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                //============================ Model\Pencetakan\SSPDTable
                $ar_PencatatanSetoran = $this->getSSPD()->getDataPencatatanSetoranBulanan($data_get->tgl_setor1, $data_get->tgl_setor2); //getDataPencatatanSetoran
                $ar_tglcetak = $data_get->tgl_cetakan;
                //$ar_periodespt = $base->periode_spt;
                $ar_pemda = $this->getPemda()->getdata();
            }
            $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
            $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->mengetahui_periksa);
        }



        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'PencatatanSetoran');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');

        $pdf->setVariables(array(
            'data_pencatatanSetoran' => $ar_PencatatanSetoran,
            'tgl_cetakan' => $ar_tglcetak,
            'tgl_setor1' => $data_get->tgl_setor1,
            'tgl_setor2' => $data_get->tgl_setor2,
            'data_pemda' => $ar_pemda,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat
        ));
        return $pdf;
    }

    public function cetakbuktivalidasipembayaranAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $datavalidasipembayaran = $this->getSSPD()->ambildatainsptvalidasi($data_get->t_idspt);
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'ValidasiPembayaran');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'datavalidasipembayaran' => $datavalidasipembayaran
        ));
        return $pdf;
    }

    public function cetakbuktipembayaranAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $datapembayaran = $this->getSSPD()->ambildatainsptvalidasi($data_get->t_idspt);
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'ValidasiPembayaran');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'datapembayaran' => $datapembayaran,
            'ar_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    public function cetakpembayaranxmlAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                //============================ Model\Pencetakan\SSPDTable
                $ar_Setoranxml = $this->getSSPD()->getDataSetoranXML($data_get->tgl_setorxml1, $data_get->tgl_setorxml2);
                $ar_tglcetakxml = $data_get->tgl_cetakxml;
            }
        }
        $view = new ViewModel(array(
            'data_Setoranxml' => $ar_Setoranxml,
            'tgl_cetakxml' => $ar_tglcetakxml,
            'tgl_setorxml1' => $data_get->tgl_setorxml1,
            'tgl_setorxml2' => $data_get->tgl_setorxml2,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function getSSPD()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }


    public function getTblPembayaran()
    {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function getPendataanBphtb()
    {
        if (!$this->tbl_pendataan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pendataan = $sm->get('SSPDBphtbTable');
        }
        return $this->tbl_pendataan;
    }



    public function getTblJenTran()
    {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan()
    {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function populateComboJenisTransaksi()
    {
        $data = $this->getTblJenTran()->comboBox();
        // var_dump($data);exit();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row["s_idjenistransaksi"]] = $row["s_namajenistransaksi"];
        }
        return $selectData;
    }

    public function populateComboHakTanah()
    {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getTblPejabat()
    {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }
}
