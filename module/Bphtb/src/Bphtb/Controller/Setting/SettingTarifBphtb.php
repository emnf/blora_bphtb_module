<?php

namespace Bphtb\Controller\Setting;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SettingTarifBphtb extends \Zend\Mvc\Controller\AbstractActionController {

    protected $tbl_pemda, $tbl_tarifbphtb;
    
    public function cekurl()
     {
        $basePath = $this->getRequest()->getBasePath();
            $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
            $uri->setPath($basePath);
            $uri->setQuery(array());
            $uri->setFragment('');
            
        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    
     }

    public function indexAction() {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Setting\TarifBphtbFrm();
        $view = new \Zend\View\Model\ViewModel(array('form' => $form));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_tarifbphtb' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function dataGridAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser']; 
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        
        $sTable = 's_tarifbphtb';
        $count = 's_idtarifbphtb';
        
        $input = $this->getRequest();
        $order_default = " s_idtarifbphtb DESC";
        $aColumns = array('s_idtarifbphtb', 's_tarifbphtb', 's_dasarhukumtarifbphtb','s_tanggaltarifbphtb','s_statustarifbphtb');
        
        $panggildata = $this->getServiceLocator()->get("TarifBphtbTable");
        $rResult = $panggildata->semuadatatarifbphtb($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());    
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }
    
    
    
    public function crudAction() {
        $input = $this->getRequest();
        $panggildata = $this->getServiceLocator()->get("TarifBphtbTable");
        switch ($input->getPost('type')) {

            //Tampilkan Data 
            case "get":
                

                $query_pilih = "SELECT * FROM s_tarifbphtb WHERE s_idtarifbphtb=" . $input->getPost('id') . "";
                $return = $panggildata->ambilsatudata($query_pilih);
                
                

                echo json_encode($return);
                break;


            //Tambah Data	
            case "new":

                $query_pilih = "INSERT INTO s_tarifbphtb (s_tarifbphtb, s_dasarhukumtarifbphtb, s_tanggaltarifbphtb, s_statustarifbphtb) values 
                                ('" . $input->getPost('s_tarifbphtb') . "', '" . $input->getPost('s_dasarhukumtarifbphtb') . "', '".$input->getPost('s_tanggaltarifbphtb')."-01-01', " . $input->getPost('s_statustarifbphtb') . ")";
                $return = $panggildata->simpandata($query_pilih);
                //var_dump($query_pilih);
                //exit();
                
                //========== yang lain di nonaktifkan
                if($input->getPost('s_statustarifbphtb') == 2){
                    $caricoy = "SELECT * FROM s_tarifbphtb order by s_idtarifbphtb desc";
                    $cariidterakir = $panggildata->ambilsatudata($caricoy);
                    
                    $query_pilih_lain = "UPDATE s_tarifbphtb SET s_statustarifbphtb=1 WHERE s_idtarifbphtb !='" . $cariidterakir['s_idtarifbphtb'] . "'";
                    $panggildata->simpandata($query_pilih_lain);
                }
                
                
                if ($return) {
                    //echo json_encode("OK");
                    echo json_encode(array("ok"=>'Berhasil Di Disimpan'));
                }
                break;

            //Edit Data	
            case "edit":

                
                $query_pilih = "UPDATE s_tarifbphtb SET 
										s_tarifbphtb='" . $input->getPost('s_tarifbphtb') . "',  
										s_dasarhukumtarifbphtb='" . $input->getPost('s_dasarhukumtarifbphtb') . "',
                                                                                s_tanggaltarifbphtb='" . $input->getPost('s_tanggaltarifbphtb') . "-01-01',   
										s_statustarifbphtb=" . $input->getPost('s_statustarifbphtb') . "  
									WHERE s_idtarifbphtb=" . $input->getPost('id') . "";
                //var_dump($query_pilih);
                //exit();
                //========== yang lain di nonaktifkan
                if($input->getPost('s_statustarifbphtb') == 2){
                    $query_pilih_lain = "UPDATE s_tarifbphtb SET s_statustarifbphtb=1 WHERE s_idtarifbphtb !=" . $input->getPost('id') . "";
                    $panggildata->simpandata($query_pilih_lain);
                }
                
                $return = $panggildata->simpandata($query_pilih);
                if ($return) {
                    //echo json_encode("OK");
                    echo json_encode(array("ok"=>'Berhasil Di Update'));
                }
                break;

            //Hapus Data	
            case "delete":

                
                $return = $this->getTbl()->hapusData($input->getPost('id'));
                
                if ($return) {
                    echo json_encode("OK");
                    
                }
                break;
        }
        exit();
    }

    public function dataGrid2Action() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new \Bphtb\Model\Setting\TarifBphtbBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getTbl()->getGridCount($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->getTbl()->getGridData($base, $start);
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['s_tarifbphtb'] . " % </td>";
            $s .= "<td>" . date('Y', strtotime($row['s_tanggaltarifbphtb'])) . "</td>";
            $s .= "<td>" . $row['s_dasarhukumtarifbphtb'] . "</td>";
            if ($row["s_statustarifbphtb"] == 0) {
                $s .= "<td>Tidak Aktif</td>";
            } else if ($row['s_statustarifbphtb'] == 1) {
                $s .= "<td>Aktif</td>";
            }
            $s .= "<td><center><a href='setting_tarif_bphtb/edit?s_idtarifbphtb=$row[s_idtarifbphtb]' class='btn btn-warning btn-sm btn-flat' style='width:55px'>Edit</a> <a href='#' onclick='hapus(" . $row['s_idtarifbphtb'] . ");return false;' class='btn btn-danger btn-sm btn-flat' style='width:55px'>Hapus</a></center></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction() {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $frm = new \Bphtb\Form\Setting\TarifBphtbFrm();
        if ($this->getRequest()->isPost()) {
            $base = new \Bphtb\Model\Setting\TarifBphtbBase();
            $frm->setInputFilter($base->getInputFilter());
            $frm->setData($this->getRequest()->getPost());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $this->getTbl()->savedata($base);
                return $this->redirect()->toRoute('setting_tarif_bphtb');
            }
        }
        $view = new \Zend\View\Model\ViewModel(array("frm" => $frm));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_tarifbphtb' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function editAction() {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $frm = new \Bphtb\Form\Setting\TarifBphtbFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('s_idtarifbphtb');
            $data = $this->getTbl()->getDataId($id);
            $frm->bind($data);
        }
        $view = new \Zend\View\Model\ViewModel(array(
            'frm' => $frm
        ));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_tarifbphtb' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusAction() {
        $this->gettbl()->hapusData($this->params('page'));
        return $this->getResponse();
    }

    public function getTbl() {
        if (!$this->tbl_tarifbphtb) {
            $this->tbl_tarifbphtb = $this->getServiceLocator()->get("TarifBphtbTable");
        }
        return $this->tbl_tarifbphtb;
    }

    public function getPemda() {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

}
