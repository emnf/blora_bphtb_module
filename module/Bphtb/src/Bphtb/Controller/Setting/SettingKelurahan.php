<?php

namespace Bphtb\Controller\Setting;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SettingKelurahan extends AbstractActionController {

    protected $tbl_kelurahan;
    protected $tbl_kecamatan;
    protected $tbls;

    public function indexAction() {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $sinkrondata_btn = false;
        if($this->getTbl()->checkEmpty()<=0 && $this->getTblKec()->checkEmpty()>0){
            $sinkrondata_btn = true;           
        }
        $view = new ViewModel(array('sinkrondata_btn'=>$sinkrondata_btn));
        $menu = new ViewModel(array('menu_setting' => 'active', 'role_id' => $session['s_akses']));
        $sidemenu = new ViewModel(array('side_setting' => 'active',
            'side_kelurahan' => 'active', 'role_id' => $session['s_akses']));
        $menu->setTemplate('bphtb/menu.phtml');
        $sidemenu->setTemplate('bphtb/side_menu.phtml');
        $view->addChild($menu, 'menu');
        $view->addChild($sidemenu, 'sidemenu');
        return $view;
    }

    public function dataGridAction() {
        $frm = new \Bphtb\Form\Setting\KelurahanFrm($this->populateComboKecamatan());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isGet()) {
            $ex = new \Bphtb\Model\Setting\KelurahanBphtbBase();
            $frm->setData($req->getQuery());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $page = $ex->page;
                $limit = $ex->rows;
                $sidx = $ex->sidx;
                $sord = $ex->sord;
                if (!$sidx)
                    $sidx = 1;
                $count = $this->getTbl()->getGridCount($ex);
                if ($count > 0 && $limit > 0) {
                    $total_pages = ceil($count / $limit);
                } else {
                    $total_pages = 0;
                }

                if ($page > $total_pages)
                    $page = $total_pages;
                $start = $limit * $page - $limit;
                if ($start < 0)
                    $start = 0;
                $res->getHeaders()->addheaders(array('Content-type' => 'text/xml'));
                $s = "<?xml version='1.0' encoding='utf-8'?>";
                $s .= "<rows>";
                $s .= "<page>" . $page . "</page>";
                $s .= "<total>" . $total_pages . "</total>";
                $s .= "<records>" . $count . "</records>";
                $data = $this->getTbl()->getGridData($ex, $start);
                foreach ($data as $row) {
                    $s .= "<row id='" . $row['s_idkelurahan'] . "'>";
                    $s .= "<cell>" . $row['s_kodekecamatan'] . "</cell>";
                    $s .= "<cell>" . $row['s_kodekelurahan'] . "</cell>";
                    $s .= "<cell>" . $row['s_namakecamatan'] . "</cell>";
                    $s .= "<cell>" . $row['s_namakelurahan'] . "</cell>";
                    $s .= "<cell><![CDATA[<a href='setting_kelurahan_bphtb/edit?s_idkelurahan=$row[s_idkelurahan]'>Edit</a>]]> || <![CDATA[<a href='#' onclick='hapus(" . $row['s_idkelurahan'] . ");return false;' >Hapus</a>]]></cell>";
                    $s .= "</row>";
                }
                $s .= "</rows>";
                $res->setContent($s);
            }
        }
        return $res;
    }

    public function tambahAction() {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Setting\KelurahanFrm($this->populateComboKecamatan());
        $req = $this->getRequest();

        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Setting\KelurahanBphtbBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                $this->getTbl()->savedata($kb);
                return $this->redirect()->toRoute('setting_kelurahan_bphtb');
            }
        }
        $view = new ViewModel(array(
            'frm' => $frm
        ));
        $menu = new ViewModel(array('menu_setting' => 'active', 'role_id' => $session['s_akses']));
        $sidemenu = new ViewModel(array('side_setting' => 'active',
            'side_kelurahan' => 'active', 'role_id' => $session['s_akses']));
        $menu->setTemplate('bphtb/menu.phtml');
        $sidemenu->setTemplate('bphtb/side_menu.phtml');
        $view->addChild($menu, 'menu');
        $view->addChild($sidemenu, 'sidemenu');
        return $view;
    }

    public function editAction() {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Setting\KelurahanFrm($this->populateComboKecamatan());
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('s_idkelurahan');
            $data = $this->getTbl()->getDataId($id);
            $frm->bind($data);
        }
        $view = new ViewModel(array(
            'frm' => $frm
        ));
        $menu = new ViewModel(array('menu_setting' => 'active', 'role_id' => $session['s_akses']));
        $sidemenu = new ViewModel(array('side_setting' => 'active',
            'side_kelurahan' => 'active', 'role_id' => $session['s_akses']));
        $menu->setTemplate('bphtb/menu.phtml');
        $sidemenu->setTemplate('bphtb/side_menu.phtml');
        $view->addChild($menu, 'menu');
        $view->addChild($sidemenu, 'sidemenu');
        return $view;
    }

    public function hapusAction() {
        $frm = new \Bphtb\Form\Setting\KelurahanFrm(NULL);
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Setting\KelurahanBphtbBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                $this->getTbl()->hapusData($kb);
            }
        }
        return $res;
    }

    public function populateComboKecamatan() {
        $data = $this->getTblKec()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idkecamatan] = $row->s_namakecamatan;
        }
        return $selectData;
    }

    public function sinkrondataAction(){
        
        if($this->getTbl()->checkEmpty()<=0 && $this->getTblKec()->checkEmpty()>0){
            $ar_kelurahan = $this->getTbls('PBBKelurahanTable')->getKelurahan();
            foreach ($ar_kelurahan as $key => $value) {
                
                $lurah_ar['s_idkecamatan'] = $this->getIdKecamatan($value->KD_KECAMATAN)->s_idkecamatan;
                $lurah_ar['s_kodekelurahan'] = $value->KD_KELURAHAN;
                $lurah_ar['s_namakelurahan'] = $value->NM_KELURAHAN;
                $this->getTbl()->save($lurah_ar);
            }           
        }
        
        return $this->redirect()->toRoute('setting_kelurahan_bphtb');
    }

    private function getIdKecamatan($kd){
        return $this->getTblKec()->getIdKecamatan($kd);
    }

    public function getTbl() {
        if (!$this->tbl_kelurahan) {
            $sm = $this->getServiceLocator();
            $this->tbl_kelurahan = $sm->get('KelurahanBphtbTable');
        }
        return $this->tbl_kelurahan;
    }

    public function getTbls($tbAlias){
        if(!$this->tbls){
            $sm = $this->getServiceLocator();
            $this->tbls = $sm->get($tbAlias);
        }
        return $this->tbls;
    }
     
    public function getTblKec() {
        if (!$this->tbl_kecamatan) {
            $sm = $this->getServiceLocator();
            $this->tbl_kecamatan = $sm->get('KecamatanBphtbTable');
        }
        return $this->tbl_kecamatan;
    }

}
