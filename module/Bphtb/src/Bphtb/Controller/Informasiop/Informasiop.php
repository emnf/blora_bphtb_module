<?php

namespace Bphtb\Controller\Informasiop;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Bphtb\Form\Pendataan\SSPDFrm;

class Informasiop extends AbstractActionController
{

    protected $tbl_pemda, $tbl_pejabat, $tbl_notaris, $tbl_nop;

    public function indexAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $Pencetak = $this->getTblPejabat()->getdata();
        $frm = new SSPDFrm();

        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        if ($s_tipe_pejabat == 2) {

            $cekpencentak = 1;
        } else {

            $cekpencentak = 2;
        }

        $view = new ViewModel(array(
            'frm' => $frm,
            'Pencetak' => $Pencetak,
            'cekpencentak' => $cekpencentak
        ));
        $data = array(
            'menu_informasiop' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cariinformasiopAction()
    {
        $frm = new SSPDFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isGet()) {
            $ex = new \Bphtb\Model\Pendataan\SPPTBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                //================== Model\Pendataan\SPPTTable
                $nopcari = $req->getQuery()->get('nop');
                $tahuncari = $req->getQuery()->get('tahun');

                $data_table = $this->getTblSPPT()->temukanDataInfoop($nopcari, $tahuncari); //$ex

                $data_tunggakan = $this->getTblSPPT()->temukanDataTunggakanopInfo($nopcari); //$ex
                /*$html = "<div class='row'>
                         <div class='col-md-12'>
                         <div id='callout-navbar-overflow' class='bs-callout bs-callout-warning'>
                         <div class='form-horizontal form-group col-md-12'>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>Nama SPPT</label>
                            <div class='col-md-3'> : " . $data_table['NM_WP_SPPT'] . "</div>
                         </div>";
                $html .= "<div class='form-horizontal form-group col-md-12'>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>Letak</label>
                            <div class='col-md-3'> : " . $data_table['JALAN_OP'] . "</div>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>RT/RW</label>
                            <div class='col-md-3'> : " . $data_table['RT_OP'] . " / " . $data_table['RW_OP'] . "</div>
                         </div>";
                $html .= "<div class='form-horizontal form-group col-md-12'>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>Kelurahan</label>
                            <div class='col-md-3'> : " . $data_table['NM_KELURAHAN'] . "</div>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>Kecamatan</label>
                            <div class='col-md-3'> : " . $data_table['NM_KECAMATAN'] . "</div>
                         </div>";
                $html . "<hr>";
                $html .= "<div class='form-horizontal form-group col-md-12'>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>Luas Tanah</label>
                            <div class='col-md-3'> : " . $data_table['LUAS_BUMI_SPPT'] . "</div>  
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>NJOP Tanah</label>
                            <div class='col-md-3'> : " . number_format($data_table['NILAI_PER_M2_TANAH'] * 1000, 0, ',', '.') . "</div>
                         </div>";
                $html .= "<div class='form-horizontal form-group col-md-12'>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>Luas Bangunan</label>
                            <div class='col-md-3'> : " . $data_table['LUAS_BNG_SPPT'] . "</div>
                            <label for='inputKey' class='col-md-2 control-label' style='text-align: left;'>NJOP Bangunan</label>
                            <div class='col-md-3'> : " . number_format($data_table['NILAI_PER_M2_BNG'] * 1000, 0, ',', '.') . "</div>
                         </div></div></div> </div>";

                //================== Model\Pendataan\SPPTTable
                
                $html .= "<div class='row'>
                          <div class='col-md-12'>
                          <div class='panel panel-primary'>
                          <div class='panel-heading'><strong>Tunggakan SPPT-PBB</strong></div>
                          <table class='table table-striped'>";
                $html .= "<tr>";
                $html .= "<th>No.</th>";
                $html .= "<th>Tahun</th>";
                $html .= "<th>Tunggakan (Rp.)</th>";
                $html .= "<th>Jatuh Tempo</th>";
                $html .= "<th>Denda (Rp.)</th>";
                $html .= "</tr>";
                $i = 1;
                $jumlahdenda = 0;
                $PBB_YG_HARUS_DIBAYAR_SPPT = 0;
                foreach ($data_tunggakan as $row) {
                    $html .= "<tr>";
                    $html .= "<td> " . $i . " </td>";
                    $html .= "<td> " . $row['THN_PAJAK_SPPT'] . " </td>";
                    $html .= "<td> " . number_format($row['PBB_YG_HARUS_DIBAYAR_SPPT'], 0, ',', '.') . " </td>";
                    $html .= "<td> " . $row['JATUH_TEMPO'] . " </td>";
//                    $html .= "<td> " . date('d-m-Y', strtotime($row['JATUH_TEMPO'])) . " </td>";
                    $dat1 = date('Y-m-d', strtotime($row['JATUH_TEMPO']));
                    $dat2 = date('Y-m-d');

			if($dat1 > $dat2 ){
			  	$date1 = new \DateTime($dat2);
			}else{
				$date1 = new \DateTime($dat1);
			}

                    
                    $date2 = new \DateTime($dat2);
                    $interval = $date1->diff($date2);
                    $bedanya = $interval->m + ($interval->y * 12);
                    if ($bedanya > 24) {
                        $beda = 24;
                    } else {
                        $beda = $bedanya;
                    }
                    $denda = $beda * $row['PBB_YG_HARUS_DIBAYAR_SPPT'] * 2 / 100;
                    $html .= "<td> " . number_format($denda, 0, ',', '.') . " </td>";
                    $html .= "</tr>";
                    $i ++;
                    $PBB_YG_HARUS_DIBAYAR_SPPT = $PBB_YG_HARUS_DIBAYAR_SPPT + $row['PBB_YG_HARUS_DIBAYAR_SPPT'];
                    $jumlahdenda = $jumlahdenda + $denda;
                }
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='2'><center>Jumlah Tunggakan</center></td>";
                $html .= "<td> " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT, 0, ',', '.') . " </td>";
                $html .= "<td>Jumlah Denda</td>";
                $html .= "<td> " . number_format($jumlahdenda, 0, ',', '.') . " </td>";
                $html .= "</tr>";
                $html .= "</table>
                        </div></div></div>";
                $data['infoop'] = $html;

                $res->setContent(\Zend\Json\Json::encode($data));*/

                $view = new ViewModel(array(
                    'data_table' => $data_table,
                    'data_tunggakan' => $data_tunggakan

                ));
                $data = array(
                    'nilai' => '3'
                );
                $this->layout()->setVariables($data);
                return $view;
            }
        }
        return $res;
    }

    public function cetaktunggakanpbbAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $datapbb = $this->getTblSPPT()->temukanDataInfoopcetak($data_get);
                $data_tunggakan = $this->getTblSPPT()->temukanDataTunggakanopcetak($data_get);

                // $s_iduser = $session['s_iduser'];
                $s_tipe_pejabat = $session['s_tipe_pejabat'];
                // var_dump($s_tipe_pejabat);exit();
                if ($s_tipe_pejabat == 2) {
                    //                    $Pencetak = $this->getTblPejabat()->getdataidnotaris($s_iduser);
                    $cekpencentak = 1;
                } else {
                    $Pencetak = $this->getTblPejabat()->getdataid((int) $data_get->Pencetak);
                    $cekpencentak = 2;
                }
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'cetaktunggakan.pdf');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'nop' => $data_get->t_nopbphtbspptinfoop,
            'datapbb' => $datapbb,
            'data_tunggakan' => $data_tunggakan,
            'Pencetak' => $Pencetak,
            'cekpencentak' => $cekpencentak
        ));
        return $pdf;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getTblPejabat()
    {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getTblNotaris()
    {
        if (!$this->tbl_notaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_notaris = $sm->get("NotarisBphtbTable");
        }
        return $this->tbl_notaris;
    }

    public function getTblSPPT()
    {
        if (!$this->tbl_nop) {
            $sm = $this->getServiceLocator();
            $this->tbl_nop = $sm->get('SPPTTable');
        }
        return $this->tbl_nop;
    }
}
