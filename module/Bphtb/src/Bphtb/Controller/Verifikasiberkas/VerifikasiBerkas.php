<?php

// Modul Verifikasi Berkas

namespace Bphtb\Controller\Verifikasiberkas;

use ArrayObject;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Math\Rand;
use Bphtb\Form\Pendataan\SSPDFrm;
use Bphtb\Model\Pendataan\SSPDBphtbBase;

class VerifikasiBerkas extends AbstractActionController
{

    protected $tbl_pemda, $tbl_pembayaran, $tbl_verifikasi, $tbl_jenistransaksi, $tbl_haktanah, $tbl_pendataan, $tbl_persyaratan, $tbl_notifikasi, $tbl_pejabat, $tbl_sspd, $tbl_nop;

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'

    }

    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Verifikasiberkas\VerifikasiBerkasFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $Mengetahuibphtb = $this->getTblPejabat()->getdata();
        $Mengetahuilengkap = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian1 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian2 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian3 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian4 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian5 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian6 = $this->getTblPejabat()->getdata();

        $data_mengetahuilengkap_sspd = $this->getTblPejabat()->getdata();
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $view = new ViewModel(array(
            "form" => $form,
            'data_mengetahuibphtb' => $Mengetahuibphtb,
            'data_mengetahuilengkap' => $Mengetahuilengkap,
            'data_mengetahuipenelitian1' => $Mengetahuipenelitian1,
            'data_mengetahuipenelitian2' => $Mengetahuipenelitian2,
            'data_mengetahuipenelitian3' => $Mengetahuipenelitian3,
            'data_mengetahuipenelitian4' => $Mengetahuipenelitian4,
            'data_mengetahuipenelitian5' => $Mengetahuipenelitian5,
            'data_mengetahuipenelitian6' => $Mengetahuipenelitian6,
            'data_mengetahuilengkap_sspd' => $data_mengetahuilengkap_sspd,
            'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_verifikasiberkas' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_pendaftaran_v6';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_idpembayaranspt DESC";
        $order_default_thn = " t_tglverifikasiberkas DESC";
        $aColumns = array('t_idspt', 't_kohirspt', 't_kohirketetapanspt', 't_tglverifikasiberkas', 's_idjenistransaksi', 't_namawppembeli', 'status_pendaftaran', 'status_berkas', 't_tglverifikasispt',  't_totalspt', 't_kodebayarbanksppt', 't_statusbayarspt',  'status_validasi',  's_namajenistransaksi', 't_persyaratan', 't_verifikasispt', 't_idjenistransaksi', 't_inputbpn', 't_periodespt', 't_idpembayaranspt', 'p_totalspt', 'p_idpemeriksaan', 'fr_tervalidasidua', 'fr_validasidua', 't_idsptsebelumnya');


        $panggildata = $this->getServiceLocator()->get("VerifikasiBerkasTable");
        $rResult = $panggildata->semuadatavalidasi($sTable, $count, $input, $order_default, $order_default_thn, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGrid2Action()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $base = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getTblPembayaran()->getGridCountVerifikasi($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->getTblPembayaran()->getGridDataVerifikasi($base, $start);
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            if ($row['t_inputbpn'] == true) {
                $s .= "<td> <span class='badge' style='background-color:#CC0000;'>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . " </span></td>";
            } else {
                $s .= "<td>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</td>";
            }
            $s .= "<td>" . $row['t_periodespt'] . "</td>";
            $s .= "<td>" . date('d-m-Y', strtotime($row['t_tglverifikasispt'])) . "</td>";
            $s .= "<td>" . $row['t_namawppembeli'] . "</td>";
            $s .= "<td>" . $row['s_namajenistransaksi'] . "</td>";
            if ($row['t_statusbayarspt'] == true) {
                $status_bayar = "Sudah Dibayar";
            } else {
                $status_bayar = "Belum Dibayar";
            }

            if (!empty($row['p_idpemeriksaan'])) {
                $cetaksuratpenelitian = "<a href='#' onclick='openCetakPenelitian(" . $row['t_kohirspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:100px'>Surat Penelitian</a>";
            } else {
                $cetaksuratpenelitian = "<a href='#' onclick='openCetakPenelitian(" . $row['t_kohirspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:100px'>Surat Penelitian</a>";
            }

            $result_array_syarat = \Zend\Json\Json::decode($row['t_persyaratan']);
            $jml_syarat = count($result_array_syarat);
            $result_array_syarat_verifikasi = \Zend\Json\Json::decode($row['t_verifikasispt']);
            $jml_syarat_verifikasi = count($result_array_syarat_verifikasi);
            $cektabelpersyaratan = $this->getTblJenTran()->jumlahsyarat($row['t_idjenistransaksi']);
            if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
                $status_verifikasi = "Tervalidasi";
                $cetaksurat = "<a href='#' onclick='openCetak(" . $row['t_kohirspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>Surat Bukti</a>";
                // $cetaksurat = "<a href='cetak_sspd/cetakbuktipenerimaanvalidasi?&action=cetakpenerimaanvalidasi&t_idspt=$row[t_idspt]' target='_blank'>Surat Bukti</a>";
                //$cetaksspd_kodebayar = "<td>" . $cetaksurat . " <a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a> " . $cetaksuratpenelitian . " </td>";
                $cetaksurat_sspd = "<a href='#' onclick='openCetakSSPD(" . $row['t_idspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a>";
                $cetaksspd_kodebayar = "<td>" . $cetaksurat . " " . $cetaksurat_sspd . " " . $cetaksuratpenelitian . " </td>";
                if ($row['t_statusbayarspt'] == true) {
                    $edit = "<td><a href='verifikasi_berkas/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a></td>";
                } else {
                    $edit = "<td><a href='verifikasi_berkas/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a> <a href='verifikasi_berkas/edit?t_idpembayaranspt=" . $row['t_idpembayaranspt'] . "' class='btn btn-warning btn-sm btn-flat'>Edit</a></td>";
                }
            } else {
                $status_verifikasi = "Belum Lengkap";
                $cetaksurat_sspd = "<a href='#' onclick='openCetakSSPD(" . $row['t_idspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a>";
                //<a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat' style='width:130px'>SSPD</a>
                $cetaksurat = "<a href='#' onclick='openCetakBukti(" . $row['t_kohirspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:130px'>Surat Pemberitahuan</a> " . $cetaksurat_sspd . "";
                // $cetaksurat = "<a href='cetak_sspd/cetakbuktipenerimaanvalidasi?&action=cetakpenerimaanvalidasi&t_idspt=$row[t_idspt]' target='_blank'>Surat Pemberitahuan</a> || <a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank'>SSPD</a>";
                $cetaksspd_kodebayar = "<td>" . $cetaksurat . " " . $cetaksuratpenelitian . "</td>";
                if ($row['t_inputbpn'] == true) {
                    $edit = "<td><a href='verifikasi_berkas/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a> <a href='verifikasi_berkas/edit?t_idpembayaranspt=" . $row['t_idpembayaranspt'] . "' class='btn btn-warning btn-sm btn-flat'>Edit</a></td>";
                } else {
                    $edit = "<td><a href='verifikasi_berkas/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a> <a href='verifikasi_berkas/edit?t_idpembayaranspt=" . $row['t_idpembayaranspt'] . "' class='btn btn-warning btn-sm btn-flat'>Edit</a> <a href='#' onclick='hapus(" . $row['t_idpembayaranspt'] . ");return false;' class='btn btn-danger btn-sm btn-flat'>Batal</a></td>";
                }
            }
            $s .= "<td>" . $status_verifikasi . "</td>";
            $s .= "<td>" . $row['t_kodebayarbanksppt'] . "</td>";
            $s .= "<td>" . $status_bayar . "</td>";
            $s .= $cetaksspd_kodebayar;
            $s .= "" . $edit . "";

            if (($session['s_namauserrole'] == "Administrator")) {
                $s .= "<td><a href='#' onclick='hapus(" . $row['t_idpembayaranspt'] . ");return false;' class='btn btn-danger btn-sm btn-flat'>Hapus Verifikasi</a></td>";
            } else {
            }

            $s .= "<tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $ar_pemda = $this->getPemda()->getdata();
        $frm = new \Bphtb\Form\Verifikasiberkas\VerifikasiBerkasFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), null, null, null, $this->getServiceLocator()->get("FormatPesanTable")->comboBox());
        $req = $this->getRequest();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Verifikasiberkas\VerifikasiBerkasBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                $t_pejabatverifikasi = $session['s_iduser'];
                $this->getTblVerifikasiBerkas()->savedata($kb, $t_pejabatverifikasi);
                return $this->redirect()->toRoute('verifikasi_berkas');
            } else {
                var_dump($frm->getMessages());
                exit();
            }
        }

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();

        $view = new ViewModel(array(
            'frm' => $frm,
            'datajenistransaksi' => $datajenistransaksi,
            'datanotaris' => $ar_notaris
        ));
        $data = array(
            'menu_verifikasiberkas' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }


    public function editAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $req = $this->getRequest();
        $t_idjenistransaksi = 0;
        $verifikasi = null;
        if ($req->isGet()) {
            $data = $this->getTblPembayaran()->getDataId($req->getQuery()->get('t_idpembayaranspt'));
            $frm = new \Bphtb\Form\Verifikasiberkas\VerifikasiBerkasFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $string, null, $this->populateCheckBoxverifikasi($data["t_idjenistransaksi"]), $this->getServiceLocator()->get("FormatPesanTable")->comboBox());
            $data["t_tglverifikasispt"] = date('d-m-Y');
            $data["t_persyaratanverifikasi"] = $data["t_verifikasiberkas"];
            $verifikasi = $data["t_verifikasiberkas"];
            $t_idjenistransaksi = $data["t_idjenistransaksi"];
            $frm->bind(new ArrayObject($data));
            // $frm->get("t_persyaratanverifikasi")->setValue(\Zend\Json\Json::decode($data["t_persyaratanverifikasi"]));
        }
        $persyaratanVerifikasi = $this->populatePersyaratanVerifikasi($t_idjenistransaksi, $verifikasi);
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();

        $view = new ViewModel(array(
            'frm' => $frm,
            'datajenistransaksi' => $datajenistransaksi,
            'datanotaris' => $ar_notaris,
            'persyaratanVerifikasi' => $persyaratanVerifikasi
        ));
        $data = array(
            'menu_verifikasiberkas' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function viewdataAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');

            //=========== persyaratan
            $combosyarat = $this->getTblPersyaratan()->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);
        }
        $view = new ViewModel(array(
            'datasspd' => $data,
            'combosyarat' => $combosyarat
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusAction()
    {
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ret = $this->getTblVerifikasiBerkas()->batalVerifikasiBerkas($this->params('page'));
            $this->getTblVerifikasiBerkas()->hapusDataPemeriksaanBerkas($this->params('page'));
            $res->setContent(\Zend\Json\Json::encode($ret));
        }
        return $res;
    }

    public function dataGridPendataanBphtbAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_databelumdivalidasiberkas'; //'fr_pendaftaran_v5'; 
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $aColumns = array(null, 't_kohirspt', 't_kodedaftarspt', 't_tglprosesspt', 't_nopbphtbsppt', 't_namawppembeli', 't_namawppenjual', 'status_upload', 't_idjenistransaksi', 't_idnotarisspt',  's_namajenistransaksi');

        $panggildata = $this->getServiceLocator()->get("VerifikasiBerkasTable");
        $rResult = $panggildata->semuadatapendaftaran($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGridPendataanBphtb2Action()
    {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_idspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;

        //============= Model\Pendataan\SSPDBphtbTable
        $count = $this->getTblSSPDBphtb()->getGridCountBlmVerifikasi($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array(
            'Content-type' => 'text/xml'
        ));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";

        //=========== Model\Pendataan\SSPDBphtbTable
        $data = $this->getTblSSPDBphtb()->databelumverifikasiBphtb($sortname, $sortorder, $query, $qtype, $start, $rp);
        //getGridDataBlmVerifikasi
        foreach ($data as $row) {
            $s .= "<row id='" . $row[' t_idspt'] . "'>";
            $s .= "<cell>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT)
                . "</cell>";
            $s .= "<cell>" . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . "</cell>";
            $s .= "<cell>" .
                $row['t_nopbphtbsppt'] . "</cell>"; //$s .="<cell>" . $row['t_namawppembeli'] . "</cell>" ; //$s .="<cell>" .
            $row['t_namawppenjual'] . "</cell>";
            $s .= "<cell>" . addslashes(str_replace(
                '\', &amp;',
                ' ',
                htmlspecialchars($row['t_namawppembeli'])
            )) . "</cell>";
            $s .= "<cell>" . addslashes(str_replace('\',
        &amp;', ' ', htmlspecialchars($row['t_namawppenjual']))) . "</cell>";
            $s
                .= "<cell><![CDATA[<a href='#' class='btn btn-xs btn-warning' onclick='pilihPendataanSspdBphtb(" .
                $row['t_idspt'] . ");return false;' >PILIH</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    public function inputvalidasikeduaAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            //$data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            //$data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_verifikasiberkas' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function simpanvalidasikeduaAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $input = $this->getRequest();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $t_idspt = str_replace("'", "\'", $this->getRequest()->getPost('t_idspt'));
        $t_iddetailsptbphtb = str_replace("'", "\'", $this->getRequest()->getPost('t_iddetailsptbphtb'));
        $t_idpembayaranspt = str_replace("'", "\'", $this->getRequest()->getPost('t_idpembayaranspt'));


        $panggildata = $this->getServiceLocator()->get("SPTTable");
        $data = $panggildata->getSptid($t_idspt);
        $datadetail = $this->getTblSSPDBphtb()->getSemuaData($t_iddetailsptbphtb);
        $dataverifikasi = $this->getTblVerifikasiBerkas()->getSptid($t_idpembayaranspt);

        //=========================== hitung lagi data yang masuk
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") ||
            ($session['s_tipe_pejabat'] == 1)
        ) {

            $idpendaftar = $session['s_iduser'];
        } elseif ($session['s_tipe_pejabat'] == 2) {

            $idpendaftar = $session['s_iduser'];
        }



        sleep(3);
        $t_luastanah = str_ireplace(".", "", $input->getPost('p_luastanah'));
        $t_njoptanah = str_ireplace(".", "", $input->getPost('p_njoptanah'));

        $t_luasbangunan = str_ireplace(".", "", $input->getPost('p_luasbangunan'));
        $t_njopbangunan = str_ireplace(".", "", $input->getPost('p_njopbangunan'));

        $aphb_kali = $datadetail->t_tarif_pembagian_aphb_kali;
        $aphb_bagi = $datadetail->t_tarif_pembagian_aphb_bagi;

        $t_nilaitransaksispt = str_ireplace(".", "", $input->getPost('p_nilaitransaksispt'));

        if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '')
            || ($aphb_bagi == 0)
        ) {
            $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
            $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
            $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

            $t_grandtotalnjop_aphb = $t_grandtotalnjop;
        } else {
            $aphb = $aphb_kali / $aphb_bagi;

            $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
            $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
            $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

            $t_grandtotalnjop_hitung = $t_grandtotalnjop * $aphb;
            $t_grandtotalnjop_aphb = ceil($t_grandtotalnjop_hitung);
        }

        if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '')
            || ($aphb_bagi == 0)
        ) {

            if ($t_grandtotalnjop > $t_nilaitransaksispt) {
                $t_npopspt = $t_grandtotalnjop;
            } else {
                $t_npopspt = $t_nilaitransaksispt;
            }
        } else {
            if ($t_grandtotalnjop_aphb > $t_nilaitransaksispt) {
                $t_npopspt = $t_grandtotalnjop_aphb;
            } else {
                $t_npopspt = $t_nilaitransaksispt;
            }
        }



        if (!empty($datadetail->p_idpemeriksaan)) {
            $t_potonganspt = $datadetail->p_potonganspt;
        } else {
            $t_potonganspt = $datadetail->t_potonganspt;
        }

        if (($datadetail->t_potongan_waris_hibahwasiat == null) || ($datadetail->t_potongan_waris_hibahwasiat == 0)) {
            $t_potongan_waris_hibahwasiat = 0;
            $hitung_potonganwaris = 1;
        } else {
            $t_potongan_waris_hibahwasiat = $datadetail->t_potongan_waris_hibahwasiat;
            $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
        }


        $t_persenbphtb = $datadetail->t_persenbphtb; //str_ireplace(".", "", $ex->t_persenbphtb);
        if ($t_potonganspt == '0') {
            $t_potonganspt = 0;
            $t_npopkpspt = $t_npopspt;
            $t_totalspt = ceil($t_npopkpspt * $t_persenbphtb / 100 * $hitung_potonganwaris);
        } else {

            $npop = $t_npopspt;
            $npopkp = $npop - $t_potonganspt;
            if ($npopkp <= 0) {
                $t_npopkpspt = 0;
                $t_totalspt = 0;
            } else {
                $t_npopkpspt = $npopkp;
                $t_totalspt = ceil($t_npopkpspt
                    * $t_persenbphtb / 100 * $hitung_potonganwaris);
            }
        } //===========================end hitung lagi data yang masuk // nilai pembayaran sebelumnya $nilaibayarsebelumnya=$datadetail->t_nilaipembayaranspt;

        if ($t_totalspt > $nilaibayarsebelumnya) {
            $fr_statusvalidasi = 1; //============= iki kurang bayar
        } else {
            $fr_statusvalidasi = 2; //============= iki lebih bayar
        }

        //========= simpan ke tabel t_spt
        $dataspt = $panggildata->savedatavalidasikedua(
            $data,
            $idpendaftar,
            $input,
            $t_potonganspt,
            $t_totalspt,
            $aphb_kali,
            $aphb_bagi,
            $fr_statusvalidasi,
            $t_potongan_waris_hibahwasiat
        );

        $this->getTblSSPDBphtb()->savedatadetail_validasikedua(
            $datadetail,
            $dataspt->t_idspt,
            $input,
            $session,
            $t_luastanah,
            $t_njoptanah,
            $t_luasbangunan,
            $t_njopbangunan,
            $t_totalnjoptanah,
            $t_totalnjopbangunan,
            $t_grandtotalnjop,
            $t_grandtotalnjop_aphb
        );
        //$this->getTblSSPDBphtb()->savedatadetailbpn($datadetail, $dataspt->t_idspt, $input);

        $datamax = $this->getTblVerifikasiBerkas()->getmaxkohir();
        $this->getTblVerifikasiBerkas()->updatenosspd_validasikedua(
            $dataspt->t_idspt,
            $datamax['t_kohirketetapanspt']
        );

        //if ($t_totalspt > 0 && $t_totalspt > $nilaibayarsebelumnya) {
        // jika ada nominal pembayaran prosesnya : daftar dan validasi



        $this->getTblVerifikasiBerkas()->savedataverifikasi_validasikedua(
            $dataverifikasi,
            $dataspt->t_idspt,
            $datamax['t_kohirketetapanspt'],
            str_replace('-', '', date('Y-m-d'))
        ); //$dataspt->t_kohirspt
        //} else {
        // jika nihil maka prosesnya : daftar, validasi dan verifikasi otomatis
        // $this->getTblVerifikasiBerkas()->savedataverbayarbpn($dataverifikasi, $dataspt->t_idspt, $string);
        //}


        return $this->redirect()->toRoute('verifikasi_spt');
    }

    public function pilihPendataanSspdBphtbAction()
    {
        $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getTblSSPDBphtb()->getPendataanSspdBphtb($ex);

                $aphb_kali = $data['t_tarif_pembagian_aphb_kali'];
                $aphb_bagi = $data['t_tarif_pembagian_aphb_bagi'];
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi ==
                    '') || ($aphb_bagi == 0)) {
                    if ($data['t_nilaitransaksispt'] >= $data['t_grandtotalnjop']) {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    } else {
                        $data['npop'] = $data['t_grandtotalnjop'];
                    }
                } else {
                    if ($data['t_nilaitransaksispt'] >= $data['t_grandtotalnjop_aphb']) {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    } else {
                        $data['npop'] = $data['t_grandtotalnjop_aphb'];
                    }
                }


                $data['t_luastanah'] = str_ireplace('.', '', $data['t_luastanah']) / 100;
                $data['t_luasbangunan'] = str_ireplace('.', '', $data['t_luasbangunan']) / 100;

                // cari harga acuan pada tabel master s_acuan
                $nop = explode('.', $data['t_nopbphtbsppt']);
                $s_kd_propinsi = $nop[0];
                $s_kd_dati2 = $nop[1];
                $s_kd_kecamatan = $nop[2];
                $s_kd_kelurahan = $nop[3];
                $s_kd_blok = $nop[4];
                $datahargaacuan = $this->getTblVerifikasiBerkas()->getHargaAcuan(
                    $s_kd_propinsi,
                    $s_kd_dati2,
                    $s_kd_kecamatan,
                    $s_kd_kelurahan,
                    $s_kd_blok
                );
                $data['s_permetertanah'] = number_format($datahargaacuan['s_permetertanah'], 0, ',', '.');
                $njop_tanah = ($data['t_nilaitransaksispt'] - $data['t_totalnjopbangunan']) / $data['t_luastanah'];
                $analisis_a = 0;
                if ($datahargaacuan) {
                    $analisis_a = $njop_tanah / $datahargaacuan['s_permetertanah'] * 100;
                }

                if ((int) $analisis_a < 0) {
                    $harga_a = 0;
                    $hasil_a = $this->getTblVerifikasiBerkas()->getPresentase($harga_a);
                } elseif ((int) $analisis_a > 100) {
                    $harga_a = 100;
                    $hasil_a = $this->getTblVerifikasiBerkas()->getPresentase($harga_a);
                } else {
                    $hasil_a = $this->getTblVerifikasiBerkas()->getPresentase($analisis_a);
                }
                $warna = $hasil_a['warna'];

                $html = "<div class='col-md-7'>";

                if ($analisis_a == 0) {
                } else {
                    $html .= "<left>NJOP Tanah (Transaksi) / Harga Acuan x 100 % = <span class='badge'
                            style='background-color:$warna;'>" . round($analisis_a, 3) . " % </span> <strong>" .
                        $hasil_a['s_keterangan'] . "</strong> </left> ";
                }
                $html .= "</div>";
                $data['analisishargaacuan'] = $html;

                $idspt = (int) $data['t_idspt'];
                $datahistorynjoptanah = $this->getTblVerifikasiBerkas()->getHargaHistoryNJOPTanahpilih(
                    $s_kd_propinsi,
                    $s_kd_dati2,
                    $s_kd_kecamatan,
                    $s_kd_kelurahan,
                    $s_kd_blok,
                    $idspt
                );

                $njoptanahtransaksi = (int) $datahistorynjoptanah['njoptanahtransaksi'];
                $njoptanah = (int) $datahistorynjoptanah['njoptanah'];
                if ($njoptanahtransaksi >= $njoptanah) {
                    $data['historynjoptanah'] = $njoptanahtransaksi;
                } else {
                    $data['historynjoptanah'] = $njoptanah;
                }

                $analisis_b = 0;
                if ($data['historynjoptanah']) {
                    $analisis_b = $njop_tanah / $data['historynjoptanah'] * 100;
                }
                if ((int) $analisis_b < 0) {
                    $harga_b = 0;
                    $hasil_b = $this->getTblVerifikasiBerkas()->getPresentase($harga_b);
                } elseif ((int) $analisis_b > 100) {
                    $harga_b = 100;
                    $hasil_b = $this->getTblVerifikasiBerkas()->getPresentase($harga_b);
                } else {
                    $hasil_b = $this->getTblVerifikasiBerkas()->getPresentase($analisis_b);
                }

                $warna_b = $hasil_b['warna'];
                $html1 = "<div class='col-md-7'>";
                if ($analisis_b == '0') {
                    $html1 .= "";
                } else {
                    $html1 .= "<left>NJOP Tanah (Transaksi) / Harga History x 100 % = <span class='badge'
                                style='background-color:$warna_b;'>" . round($analisis_b, 3) . " % </span> <strong>" .
                        $hasil_b['s_keterangan'] . "</strong> </left>";
                }
                $html1 .= "</div>";
                $data['analisisharganjoptanah'] = $html1;
                $data['historynjoptanah'] = number_format($data['historynjoptanah'], 0, ',', '.');

                $data['t_luastanah'] = number_format($data['t_luastanah'], 0, ',', '.');
                $data['t_njoptanah'] = number_format($data['t_njoptanah'], 0, ',', '.');
                $data['t_luasbangunan'] = number_format($data['t_luasbangunan'], 0, ',', '.');
                $data['t_njopbangunan'] = number_format($data['t_njopbangunan'], 0, ',', '.');
                $data['t_totalnjoptanah'] = number_format($data['t_totalnjoptanah'], 0, ',', '.');
                $data['t_totalnjopbangunan'] = number_format($data['t_totalnjopbangunan'], 0, ',', '.');
                $data['t_grandtotalnjop'] = number_format($data['t_grandtotalnjop'], 0, ',', '.');
                $data['t_nilaitransaksispt'] = number_format($data['t_nilaitransaksispt'], 0, ',', '.');
                $data['npop'] = number_format($data['npop'], 0, ',', '.');
                $data['t_potonganspt'] = number_format($data['t_potonganspt'], 0, ',', '.');

                $data['t_tarif_pembagian_aphb_kali'] = $data['t_tarif_pembagian_aphb_kali'];
                $data['t_tarif_pembagian_aphb_bagi'] = $data['t_tarif_pembagian_aphb_bagi'];
                $data['t_grandtotalnjop_aphb'] = number_format($data['t_grandtotalnjop_aphb'], 0, ',', '.');
                $data['p_grandtotalnjop_aphb'] = number_format($data['p_grandtotalnjop_aphb'], 0, ',', '.');
                $data['t_persenbphtb'] = $data['t_persenbphtb'];
                $data['t_potongan_waris_hibahwasiat'] = $data['t_potongan_waris_hibahwasiat'];
                $data['t_idjenistransaksi'] = $data['t_idjenistransaksi'];

                $npopkp = (str_replace('.', '', $data['npop']) - str_replace('.', '', $data['t_potonganspt']));
                if ($npopkp <= 0) {
                    $nilai_npopkp = 0;
                } else {
                    $nilai_npopkp = $npopkp;
                }
                $data['npopkp'] = number_format($nilai_npopkp, 0, ',', '.'); //$npopkp=$data['t_totalspt'] / 5 *100; //$data['npopkp']=number_format($npopkp, 0, ',' , '.' );
                $data['t_totalspt'] = number_format($data['t_totalspt'], 0, ',', '.'); // Persyaratan Pendaftaran dan Validasi 
                $data['t_persyaratan'] = $this->populatePersyaratanId($data['t_idjenistransaksi'], $data['t_idspt']);
                $data['t_persyaratanverifikasi'] = $this->populatePersyaratanVerifikasi($data['t_idjenistransaksi']);

                $panggildata = $this->getServiceLocator()->get("PersyaratanTable");
                $datafileupload = $panggildata->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);

                $no = 1;
                $namasyarat = '';
                $nomer = 1;
                $fileuploadnya = '';
                foreach ($datafileupload as $v) {

                    if ($namasyarat <> $v['s_namapersyaratan']) {

                        if ($namasyarat <> '') {
                        }
                        //$fileuploadnya .= $no;

                        $fileuploadnya .= $no . ". " . $v['s_namapersyaratan'] . "<br>";
                        /*$fileuploadnya .= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                    href="'.$this->url().'/uploadjenisyarat?t_idspt='.$datasspd['t_idspt'].'&syarat='.$v['s_idpersyaratan'].'"
                                    class="btn bg-purple btn-sm btn-flat">
                                    <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Upload File
                                </a><br>';
                                */
                        $namasyarat = $v['s_namapersyaratan'];
                        $clast_rek_name = $v['s_namapersyaratan'];
                        //$no = 1;
                        $no++;
                        $nomer = 1;
                    }

                    if (!empty($v['nama_file'])) {
                        //$fileuploadnya .= $v['nama_file']."<br>";
                        $fileuploadnya .= '<a style="display:none;"
                                    data-ng-href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '"
                                    title="' . $v['nama_file'] . '" download="' . $v['nama_file'] . '" data-gallery=""
                                    href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '"><img
                                        data-ng-src="' . $this->cekurl() . '/' . $v['letak_file'] . 'thumbnail/' . $v['nama_file'] . '"
                                        alt=""
                                        src="' . $this->cekurl() . '/' . $v['letak_file'] . 'thumbnail/' . $v['nama_file'] . '"></a>';
                        $fileuploadnya .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                    data-ng-switch-when="true"
                                    data-ng-href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '"
                                    title="' . $v['nama_file'] . '" download="' . $v['nama_file'] . '" data-gallery=""
                                    class="ng-binding ng-scope"
                                    href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '">' . $nomer . '.
                                    ' . $v['nama_file'] . '</a><br>';
                    }

                    $nomer++;
                }

                $data['fileuploadcoy'] = $fileuploadnya;

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function datapembayaranAction()
    {
        $frm = new
            \Bphtb\Form\Verifikasiberkas\VerifikasiBerkasFrm(
                $this->populateComboJenisTransaksi(),
                $this->populateComboHakTanah()
            );
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Verifikasiberkas\VerifikasiBerkasBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getTblVerifikasiBerkas()->temukanDataPembayaran($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Menghitung nilai njop pbb
    public function hitungnjopAction()
    {
        $frm = new
            \Bphtb\Form\Verifikasiberkas\VerifikasiBerkasFrm(
                $this->populateComboJenisTransaksi(),
                $this->populateComboHakTanah()
            );
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $p_luastanah = str_ireplace(".", "", $ex->p_luastanah);
                $p_njoptanah = str_ireplace(".", "", $ex->p_njoptanah);

                $p_luasbangunan = str_ireplace(".", "", $ex->p_luasbangunan);
                $p_njopbangunan = str_ireplace(".", "", $ex->p_njopbangunan);

                $input = $this->getRequest();
                $aphb_kali = $input->getPost('p_t_tarif_pembagian_aphb_kali');
                $aphb_bagi = $input->getPost('p_t_tarif_pembagian_aphb_bagi');

                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi ==
                    null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    $p_totalnjoptanah = $p_luastanah * $p_njoptanah;
                    $p_totalnjopbangunan = $p_luasbangunan * $p_njopbangunan;
                    $p_grandtotalnjop = $p_totalnjoptanah + $p_totalnjopbangunan;
                    $p_grandtotalnjop_aphb = $p_grandtotalnjop;
                } else {
                    $aphb = $aphb_kali / $aphb_bagi;
                    $p_totalnjoptanah = $p_luastanah * $p_njoptanah;
                    $p_totalnjopbangunan = $p_luasbangunan * $p_njopbangunan;
                    $p_grandtotalnjop = $p_totalnjoptanah + $p_totalnjopbangunan;

                    $p_grandtotalnjop_aphb = ceil($p_grandtotalnjop * $aphb);
                }


                $p_nilaitransaksispt = str_ireplace(".", "", $ex->p_nilaitransaksispt);
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi ==
                    null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($p_nilaitransaksispt >= $p_grandtotalnjop * 1) {
                        $p_npop = $p_nilaitransaksispt;
                    } else {
                        $p_npop = $p_grandtotalnjop;
                    }
                } else {
                    if ($p_nilaitransaksispt >= $p_grandtotalnjop_aphb * 1) {
                        $p_npop = $p_nilaitransaksispt;
                    } else {
                        $p_npop = $p_grandtotalnjop_aphb;
                    }
                }

                if (!empty($input->getPost('t_potongan_waris_hibahwasiat'))) {
                    $t_potongan_waris_hibahwasiat = $input->getPost('t_potongan_waris_hibahwasiat');
                    $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
                } else {
                    $t_potongan_waris_hibahwasiat = 0;
                    $hitung_potonganwaris = 1;
                }


                $p_potonganspt = str_ireplace(".", "", $ex->p_potonganspt);
                $p_npopkp = $p_npop - $p_potonganspt;
                if ($p_npopkp <= 0) {
                    $p_npopkp = 0;
                    $p_totalspt = 0;
                } else {
                    $p_persenbphtb = str_ireplace(".", "", $ex->p_persenbphtb);
                    $p_totalspt = ceil($p_npopkp * $p_persenbphtb / 100 * $hitung_potonganwaris);
                }
                $data = array(
                    "p_totalnjoptanah" => $p_totalnjoptanah,
                    "p_totalnjopbangunan" => $p_totalnjopbangunan,
                    "p_grandtotalnjop" => $p_grandtotalnjop,
                    "p_t_grandtotalnjop_aphb" => $p_grandtotalnjop_aphb,
                    "p_npop" => $p_npop,
                    "p_potonganspt" => $p_potonganspt,
                    "p_npopkp" => $p_npopkp,
                    "p_totalspt" => $p_totalspt
                );
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    function populatePersyaratanId($idtransaksi, $idspt)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($idtransaksi);
        $html = "<div>";
        $html .= "<label>Persyaratan Pendaftaran : </label>";
        foreach ($data as $row) {
            $html .= "<div class='col-sm-12'>
                                            <div class='form-group'>";
            $html .= "<div class='col-sm-12'>";
            $dataa =
                $this->getTblPersyaratan()->getDataSyaratSPT(
                    $row->s_idpersyaratan,
                    $idspt
                );
            if ($dataa == false) {
                $html .= "<input id='t_persyaratan' name='t_persyaratan[]'
                                                        type='checkbox' value='" . $row->s_idpersyaratan . "'>";
            } else {
                $html .= "<input id='t_persyaratan' name='t_persyaratan[]'
                                                        type='checkbox' value='" . $row->s_idpersyaratan . "'
                                                        checked='checked' disabled> ";
            }
            $html .= $row->s_namapersyaratan;
            $html .= "</div>";
            $html .= "</div>";
            $html .= "
                                        </div>";
        }
        $html .= "</div>";
        return $html;
    }

    function populatePersyaratanVerifikasi($id, $verifikasi = null)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($id);
        $countpersyaratan = count($data);
        $html = "<div>";
        $html .= "<label>Persyaratan Validasi : </label>";
        $html .= "<div class='col-sm-12'>
                                            <div class='form-group'>
                                                <div class='col-sm-12'>
                                                    <input type='checkbox' id='CheckAll' name='CheckAll'
                                                        onClick='modify_boxes($countpersyaratan)'> <span
                                                        style='color:green'>Centang Semua</span>
                                                </div>
                                            </div>
                                        </div>";
        foreach ($data as $row) {
            $html .= "<div class='col-sm-12'>
                                            <div class='form-group'>";
            $html .= "<div class='col-sm-12'>";
            $html .= "<input id='t_persyaratanverifikasi'
                                                        name='t_persyaratanverifikasi[]' type='checkbox'
                                                        value='" . $row->s_idpersyaratan . "'" . (($verifikasi != '' && $verifikasi != null && in_array($row->s_idpersyaratan, json_decode($verifikasi))) ? ' checked' : '') . "> ";
            $html .= $row->s_namapersyaratan;
            $html .= "</div>";
            $html .= "</div>";
            $html .= "
                                        </div>";
        }
        $html .= "</div>";
        return $html;
    }

    public function populateComboJenisTransaksi()
    {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row["s_idjenistransaksi"]] = $row["s_namajenistransaksi"];
        }
        return $selectData;
    }

    // Pengecekan Tunggakan PBB
    // Lokasi : Tambah Pembayaran
    public function cektunggakanpbbc()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                var_dump($ex);
                exit();
                $datahari = $this->getTblPembayaran()->cekJumlahhari();
                $data = $this->getTblPembayaran()->cekValiditasSSPD($ex, $datahari['s_jumlahhari']);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetaksuratpenelitianAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $ar_sspd = $this->getSSPD()->getdatasurathasilpenelitian($data_get);

                $ar_Mengetahui1 =
                    $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian1);
                $ar_Mengetahui2 =
                    $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian2);
                $ar_Mengetahui3 =
                    $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian3);
                $ar_Mengetahui4 =
                    $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian4);
                $ar_Mengetahui5 =
                    $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian5);
                $ar_Mengetahui6 =
                    $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian6);
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'SuratHasilPenelitian');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $data_get->tgl_cetak_penelitian,
            'data_mengetahui1' => $ar_Mengetahui1,
            'data_mengetahui2' => $ar_Mengetahui2,
            'data_mengetahui3' => $ar_Mengetahui3,
            'data_mengetahui4' => $ar_Mengetahui4,
            'data_mengetahui5' => $ar_Mengetahui5,
            'data_mengetahui6' => $ar_Mengetahui6
        ));
        return $pdf;
    }

    public function cektunggakanpbbAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $base = new \Bphtb\Model\Pendataan\SPPTBase();
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getPost();
                $base->t_nopbphtbspptinfoop = $data_get['t_nopbphtbsppt'];
                $data_tunggakan = $this->getTblSPPT()->temukanDataTunggakanop($base);
                $html = "<div class='row'>
                                        <div class='col-md-12'>
                                            <div class='panel panel-primary'>
                                                <div class='panel-heading'><strong>Tunggakan SPPT-PBB</strong></div>
                                                <table class='table table-striped'>";
                $html .= "<tr>";
                $html .= "<th>No.</th>";
                $html .= "<th>Tahun</th>";
                $html .= "<th>Tunggakan (Rp.)</th>";
                $html .= "<th>Jatuh Tempo</th>";
                $html .= "<th>Denda (Rp.)</th>";
                $html .= "</tr>";
                $i = 1;
                $jumlahdenda = 0;
                $PBB_YG_HARUS_DIBAYAR_SPPT = 0;
                foreach ($data_tunggakan as $row) {
                    $html .= "<tr>";
                    $html .= "<td> " . $i . " </td>";
                    $html .= "<td> " . $row['THN_PAJAK_SPPT'] . " </td>";
                    $html .= "<td> " .
                        number_format(
                            $row['PBB_YG_HARUS_DIBAYAR_SPPT'],
                            0,
                            ',',
                            '.'
                        ) . " </td>";
                    $html .= "<td> " . $row['JATUH_TEMPO'] . " </td>";
                    // $html .= "<td> " . date('d-m-Y', strtotime($row['JATUH_TEMPO'])) . " </td>";
                    $dat1 = date('Y-m-d', strtotime($row['JATUH_TEMPO']));
                    $dat2 = date('Y-m-d');
                    /*$date1 = new \DateTime($dat1);
                                                        $date2 = new \DateTime($dat2);
                                                        $interval = $date1->diff($date2);
                                                        $bedanya = $interval->m + ($interval->y * 12);
                                                        if ($bedanya > 24) {
                                                        $beda = 24;
                                                        } else {
                                                        $beda = $bedanya;
                                                        }*/

                    $tgl_bayar = explode("-", $dat2);
                    $tgl_tempo = explode("-", $dat1);

                    $tahun = $tgl_bayar[0] - $tgl_tempo[0];
                    $bulan = $tgl_bayar[1] - $tgl_tempo[1];
                    $hari = $tgl_bayar[2] - $tgl_tempo[2];


                    if (($tahun == 0) || ($tahun < 1)) {
                        if (($bulan == 0) || ($bulan <
                            1)) {
                            if ($bulan < 0) {
                                $months = 0;
                            } else {
                                if (($hari == 0) ||
                                    ($hari < 1)
                                ) {
                                    $months = 0;
                                } else {
                                    $months = 1;
                                }
                            }
                        } else {
                            if (($hari == 0) || ($hari < 1)) {
                                $months = $bulan;
                            } else {
                                $months = $bulan + 1;
                            }
                        }
                    } else {
                        $jmltahun = $tahun * 12;
                        if ($bulan == 0) {
                            $months = $jmltahun;
                        } elseif ($bulan < 1) {
                            $months = $jmltahun + $bulan;
                        } else {
                            $months = $bulan +
                                $jmltahun;
                        }
                    }
                    if ($months > 24) {
                        $beda = 24;
                    } else {
                        $beda = $months;
                    }

                    $denda = $beda * $row['PBB_YG_HARUS_DIBAYAR_SPPT'] * 2 /
                        100;
                    $html .= "<td> " . number_format($denda, 0, ',', '.') . "
                                                            </td>";
                    $html .= "</tr>";
                    $i++;
                    $PBB_YG_HARUS_DIBAYAR_SPPT = $PBB_YG_HARUS_DIBAYAR_SPPT +
                        $row['PBB_YG_HARUS_DIBAYAR_SPPT'];
                    $jumlahdenda = $jumlahdenda + $denda;
                }
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='2'>
                                                            <center>Jumlah Tunggakan</center>
                                                        </td>";
                $html .= "<td> " . number_format(
                    $PBB_YG_HARUS_DIBAYAR_SPPT,
                    0,
                    ',',
                    '.'
                ) . " </td>";
                $html .= "<td>Jumlah Denda</td>";
                $html .= "<td> " . number_format($jumlahdenda, 0, ',', '.') . "
                                                        </td>";
                $html .= "</tr>";
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='3'>
                                                            <center>Jumlah Seluruh Tunggakan</center>
                                                        </td>";
                $html .= "<td colspan='2'>
                                                            <div style='text-align:center; color:red'> Rp. " .
                    number_format(
                        $PBB_YG_HARUS_DIBAYAR_SPPT + $jumlahdenda,
                        0,
                        ',',
                        '.'
                    ) . " </div>
                                                        </td>";
                $html .= "</tr>";
                $html .= "</table>
                                            </div>
                                        </div>
                                    </div>";
                $data['datatunggakan'] = $html;
                $data['PBB_YG_HARUS_DIBAYAR_SPPT'] = $PBB_YG_HARUS_DIBAYAR_SPPT;
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetakkodebayarAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();

                //=============== Model\Pencetakan\SSPDTable
                $ar_sspd = $this->getSSPD()->ambildatakodebayar($data_get);
                $ar_tglcetak = $base->tgl_cetak;
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'KodeBayar');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'tgl_cetak' => $ar_tglcetak,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    public function cetakdaftarbelumbayarAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        //========================== Model\Pencetakan\SSPDTable
        $ar_DataVerifikasi =
            $this->getSSPD()->getDataBelumBayar(
                $data_get->tgl_verifikasi11,
                $data_get->tgl_verifikasi22
            );

        $ar_tglcetak = $data_get->tgl_cetak;

        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'DaftarBelumBayar');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_Verifikasi' => $ar_DataVerifikasi,
            'tgl_cetak' => $ar_tglcetak,
            'tgl_periode1' => $data_get->tgl_verifikasi11,
            'tgl_periode2' => $data_get->tgl_verifikasi22,
            'nama_login' => $session['s_namauserrole']
        ));
        return $pdf;
    }

    public function cetakdaftarkurangbayarAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        //========================== Model\Pencetakan\SSPDTable
        $ar_DataVerifikasi =
            $this->getSSPD()->getDataKurangBayar(
                $data_get->tgl_verifikasi121,
                $data_get->tgl_verifikasi222
            );

        $ar_tglcetak = $data_get->tgl_cetak;

        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'DaftarKurangBayar');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_Verifikasi' => $ar_DataVerifikasi,
            'tgl_cetak' => $ar_tglcetak,
            'tgl_periode1' => $data_get->tgl_verifikasi111,
            'tgl_periode2' => $data_get->tgl_verifikasi222,
            'nama_login' => $session['s_namauserrole']
        ));
        return $pdf;
    }

    public function cetakbuktipenerimaanvalidasiAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $datahari = $this->getTblPembayaran()->cekJumlahhari();
                if ($data_get->action == 'cetakpenerimaanvalidasi') {
                    $ar_sspd = $this->getSSPD()->cetakbuktipenerimaanvalidasi($data_get->t_idspt);
                    // $ar_sspd = $this->getSSPD()->ambildatainsptvalidasi($data_get->t_idspt);
                    $ar_tglcetak = date('d-m-Y');
                    $hasildata = $this->getSSPD()->cetakbuktipenerimaanvalidasi($data_get->t_idspt);
                } else {
                    //=============== Model\Pencetakan\SSPDTable
                    $ar_sspd = $this->getSSPD()->cetakbuktipenerimaanvalidasi($base);
                    // $ar_sspd = $this->getSSPD()->ambildatainsptvalidasi(@$data_tidspt);
                    $ar_tglcetak = $base->tgl_cetak;
                    $hasildata = $this->getSSPD()->cetakbuktipenerimaanvalidasi($base);
                }
                $ar_pemda = $this->getPemda()->getdata();
                $testarray = array();
                foreach ($hasildata as $row) {
                    $result_array_syarat = \Zend\Json\Json::decode($row['t_persyaratan']);
                    $jml_syarat = count($result_array_syarat);
                    $result_array_syarat_verifikasi =
                        \Zend\Json\Json::decode($row['t_verifikasiberkas']);
                    $jml_syarat_verifikasi = count($result_array_syarat_verifikasi);
                    if ($jml_syarat == $jml_syarat_verifikasi) {
                    } else {
                        $combosyarat = $this->getTblPersyaratan()->comboBox($row['t_idjenistransaksi']);
                        array_push($testarray, $combosyarat);
                    }
                }
            }
        }

        //var_dump($ar_sspd);
        //exit();

        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtb);
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'BuktiPenerimaan');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'tgl_cetak' => $ar_tglcetak,
            'data_pemda' => $ar_pemda,
            'testarray' => $testarray,
            'datahari' => $datahari,
            'ar_Pejabat' => $ar_Pejabat
        ));
        return $pdf;
    }

    public function cetakdaftarverifikasiberkasAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        //========================== Model\Pencetakan\SSPDTable
        $ar_DataVerifikasi =
            $this->getSSPD()->getDataVerifikasiBerkas(
                $data_get->periode_spt,
                $data_get->tgl_verifikasi1,
                $data_get->tgl_verifikasi2
            );
        $ar_tglcetak = $data_get->tgl_cetak;
        $ar_periodespt = $data_get->periode_spt;
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'DaftarVerifikasiBerkas');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'landscape');
        $pdf->setVariables(array(
            'data_Verifikasi' => $ar_DataVerifikasi,
            'tgl_cetak' => $ar_tglcetak,
            'periode_spt' => $ar_periodespt,
            'tgl_verifikasi1' => $data_get->tgl_verifikasi1,
            'tgl_verifikasi2' => $data_get->tgl_verifikasi2,
            'nama_login' => $session['s_namauserrole']
        ));
        return $pdf;
    }

    public function populateComboHakTanah()
    {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    private function populateCheckBoxverifikasi($idtransaksi)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($idtransaksi);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpersyaratan] = ' ' . $row->s_namapersyaratan;
        }
        return $selectData;
    }

    public function getTblVerifikasiBerkas()
    {
        if (!$this->tbl_verifikasi) {
            $sm = $this->getServiceLocator();
            $this->tbl_verifikasi = $sm->get('VerifikasiBerkasTable');
        }
        return $this->tbl_verifikasi;
    }

    public function getTblPembayaran()
    {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function getTblJenTran()
    {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan()
    {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getTblSSPDBphtb()
    {
        if (!$this->tbl_pendataan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pendataan = $sm->get('SSPDBphtbTable');
        }
        return $this->tbl_pendataan;
    }

    public function getTblPersyaratan()
    {
        if (!$this->tbl_persyaratan) {
            $sm = $this->getServiceLocator();
            $this->tbl_persyaratan = $sm->get('PersyaratanTable');
        }
        return $this->tbl_persyaratan;
    }

    public function getTblPejabat()
    {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getSSPD()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblSPPT()
    {
        if (!$this->tbl_nop) {
            $sm = $this->getServiceLocator();
            $this->tbl_nop = $sm->get('SPPTTable');
        }
        return $this->tbl_nop;
    }

    public function cetaksuratpermohonanpenelitianAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                //=============== Model\Pencetakan\SSPDTable
                $datapendaftaran = $this->getSSPD()->ambildatakodebayarpendaftaran($data_get);
                $ar_tglcetak = $base->tgl_cetak;
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'SuratPermohonanPenelitian');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'datapendaftaran' => $datapendaftaran,
            'tgl_cetak' => $ar_tglcetak,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    // private function getTblNotifikasi(){
    // if (!$this->tbl_notifikasi) {
    // $sm = $this->getServiceLocator();
    // $this->tbl_notifikasi = $sm->get('NotifikasiTable');
    // }
    // return $this->tbl_notifikasi;
    // }

    public function cetakformulirsurveyAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                //=============== Model\Pencetakan\SSPDTable
                $ar_sspd = $this->getSSPD()->getviewcetaformulirsurvey($data_get->t_idspt);
                $ar_tglcetak = $base->tgl_cetak;
                $ar_pemda = $this->getPemda()->getdata();
                $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtbsspd);
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'SuratFormulirSurvey');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'tgl_cetak' => $ar_tglcetak,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }
}
