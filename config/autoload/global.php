<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'db' => array(
        'adapters' => array(
            'bphtb' => array(
                'driver' => 'pdo',
                'dsn' => 'pgsql:dbname=blora_bphtb;host=localhost',
                'username' => 'postgres',
                'password' => 'postgres',
            ),
            'oracle_db' => array(
                'driver' => 'OCI8',
                'connection_string' => '192.168.56.101/SIMPBB',
                'character_set' => 'AL32UTF8',
                'username' => 'PBB',
                'password' => 'PBB',
            ),
            'espop_db' => array(
                'driver' => 'pdo',
                'dsn' => 'pgsql:dbname=elayanan2;host=localhost',
                'username' => 'postgres',
                'password' => 'postgres',
            ),
        ),
        'options' => array(
            'buffer_results' => true
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Db\Adapter\AdapterAbstractServiceFactory',
        ),
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);
